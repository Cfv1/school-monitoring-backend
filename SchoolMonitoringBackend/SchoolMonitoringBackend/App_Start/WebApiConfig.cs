﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SchoolMonitoringBackend
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Отключение XML формата. Данные будут возвращаться только в JSON
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Разрешение на отправку данных указанному порту над контроллером
            config.EnableCors();

            // Конфигурация и службы веб-API

            // Маршруты веб-API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
