﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.LowSchoolReportModels;
using SchoolMonitoringBackend.Services;
using SchoolMonitoringBackend.Models.ViewModels;
using System.Data.Entity;
using SchoolMonitoringBackend.Areas;

namespace SchoolMonitoringBackend.Controllers.LowSchoolControllers
{
  [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
  public class LowSchoolController : ApiController
  {
      SchoolContext db = new SchoolContext();

      [HttpGet]
      [Route("createclassreport")]
      public HttpResponseMessage CreateClassReport(string className, int yearId)
      {
          ReportClassroom report = ClassReportService.CreateReport(className, yearId);
          return Request.CreateResponse(HttpStatusCode.OK, report);
      }

      [HttpGet]
      [Route("createclassreport/parallel")]
      public HttpResponseMessage CreateClassReportByParallel(string className, int yearId)
      {
          ReportClassroom report = ClassReportService.CreateReportByParallel(className, yearId);  
          return Request.CreateResponse(HttpStatusCode.OK, report);
      }

      
  [HttpGet]
  [Route("createsubjectreport")]
  public HttpResponseMessage CreateSubjectReport(byte quarterName, int yearId, string className)
  {
      var reports = SubjectReportService.CreateSubjectReport(quarterName, yearId, className);
      return Request.CreateResponse(HttpStatusCode.OK, reports);
  }

      [HttpGet]
      [Route("createreadtechnickcheck")]
      public HttpResponseMessage ReadTechnickCheck(byte quarterName, int yearId, string className)
      {
          ReadTechickCheckReport report = new ReadTechickCheckReport();
          report.ClassName = className;
          var query = db.PupilReadTechniques
                      .Include(z => z.Pupil)
                      .Include(z => z.ReadTechnique)
                      .Include(z => z.Pupil.ClassPupils)
                      .Where(e => e.Pupil.ClassPupils.Any(c=> c.Class.Name == className))
                      .Where(e => e.Quarter.Name == quarterName)
                      .Where(e => e.YearOfStudyId == yearId)
                      .Select(s => s);
          foreach (var q in query)
          {
              report.SurnameName.Add(q.Pupil.Surname + " " + q.Pupil.Name);
              report.WordCount.Add(q.ReadTechnique.WordCount);
              report.Correctness.Add(q.ReadTechnique.Correctness);
              report.Mindfulness.Add(q.ReadTechnique.Mindfulness);
              report.Evaluation.Add(q.ReadTechnique.Evaluation);
          }
          report.PupilsInClass = db.Pupils.Where(e => e.ClassPupils.Any(c => c.Class.Name == className)).Count();

          var countEvaluation = 0;
          for (var i = 0; i < report.SurnameName.Count; i++)
          {
              if (report.Evaluation[i] == 5)
              {
                  report.PupilsFiveGrade++;
                  countEvaluation += 5;
              }
              else if (report.Evaluation[i] == 4)
              {
                  report.PupilsFourGrade++;
                  countEvaluation += 4;
              }
              else if (report.Evaluation[i] == 3)
              {
                  report.PupilsThreeGrade++;
                  countEvaluation += 3;
              }
              else if (report.Evaluation[i] == 2)
              {
                  report.PupilsTwoGrade++;
                  countEvaluation += 2;
              }
          }

          var without2 = report.PupilsFiveGrade + report.PupilsFourGrade + report.PupilsThreeGrade;
          var without23 = report.PupilsFiveGrade + report.PupilsFourGrade;

          report.AverageScore = Math.Round((double)countEvaluation / report.SurnameName.Count, 2);
          report.Performance = Math.Round((double)without2 / report.SurnameName.Count * 100, 2);
          report.QualityPerformance = Math.Round((double)without23 / report.SurnameName.Count
              *100, 2);

          return Request.CreateResponse(HttpStatusCode.OK, report);
      }

      [HttpGet]
      [Route("createprogressreport")]
      public HttpResponseMessage CreateProgressReport(byte quarterName, int yearId, string className)
      {
        var report = ProgressService.CreateProgressReport(quarterName, yearId, className);
        return Request.CreateResponse(HttpStatusCode.OK, report);
      }
    
    [HttpGet]
    [Route("createstartyearreport")]
    public HttpResponseMessage CreateStartYearReport(int yearId, string className)
    {
      var report = StartYearReportService.CreateStartYearReport(yearId, className);
      return Request.CreateResponse(HttpStatusCode.OK, report);
    }

    [HttpGet]
    [Route("createmathreport")]
    public HttpResponseMessage CreateMathReport(byte quarterName, int yearId, string className)
    {
      var report = MathReportService.CreateReport(quarterName, yearId, className);
      return Request.CreateResponse(HttpStatusCode.OK, report);
    }

    /*[HttpPost]
    [Route("addcontrolwork")]
    public HttpResponseMessage AddControlWork()
    {
      var quarterYearId = db.Quarters.Where(q => pupil.ArrivedDate >= q.Date_of_start
        && pupil.ArrivedDate < q.Date_of_ending)
          .Select(s => new { QuarterId = s.Id, YearId = s.YearOfStudyId }).First();
      var response = Request.CreateResponse(HttpStatusCode.Created);
      return response;
    }*/

    //для тестов
    /*[HttpGet]
      [Route("test")]
      public HttpResponseMessage Testing()
      {
          SchoolContext db = new SchoolContext();
          var classes = db.Classes
      .Where(n => n.Name.Contains("1"))
      .ToList();
          return Request.CreateResponse(HttpStatusCode.OK, classes);
      }*/
  }
}
