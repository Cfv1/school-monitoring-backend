﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.TeacherPanelModels;
using SchoolMonitoringBackend.Services;
using SchoolMonitoringBackend.Areas;
using SchoolMonitoringBackend.Models.ViewModels;

namespace SchoolMonitoringBackend.Controllers
{
  [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
  public class AdminPanelController : ApiController
  {
    SchoolContext db = new SchoolContext();

    [HttpGet]
    [Route("searchbyfio")]
    public HttpResponseMessage SearchbyFIO(string fullname)
    {
      int? teacherId;
      string[] parseFullName = fullname.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
      string surname = parseFullName[0];
      var response = new SearchByFIOModel();
      if (parseFullName.Length == 1)
      {
        teacherId = db.Teachers.Where(e => e.Surname.Contains(surname))
          .Select(s => s.Id).FirstOrDefault();
      }
      else
      {
        string name = parseFullName[1];
        teacherId = db.Teachers.Where(e => e.Surname.Contains(surname) && e.Name.Contains(name))
          .Select(s => s.Id).FirstOrDefault();
      }
      if (teacherId == null)
        return Request.CreateResponse(HttpStatusCode.OK, response);
      response.Education = db.Educations.Where(e => e.EducationTeachers.Any(v => v.TeacherId == teacherId))
        .Select(s => s.Name).FirstOrDefault();
      response.Attestation = db.Teachers.Where(e => e.Id == teacherId)
        .Select(s => s.Attestation.Name).FirstOrDefault();
      response.Rewards = db.Rewards.Where(e => e.TeacherRewards.Any(v => v.TeacherId == teacherId))
        .Select(s => s.Name).ToList();
      response.FIO = db.Teachers.Where(e => e.Id == teacherId)
        .Select(s => s.Surname + " " + s.Name + " " + s.Second_name).FirstOrDefault();

      return Request.CreateResponse(HttpStatusCode.OK, response);
    }

    [HttpGet]
    [Route("searchbycathegory")]
    public HttpResponseMessage SearchByCathegory(string cathegory)
    {
      db.Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));
      /*var teachers = (from teacher in db.Teachers
                  join educteach in db.EducationTeachers on teacher.Id equals educteach.TeacherId
                  join education in db.Educations on educteach.EducationId equals education.Id
                  join attestation in db.Attestations on teacher.AttestationId equals attestation.Id
                  join teacherreward in db.TeacherRewards on teacher.Id equals teacherreward.TeacherId
                  join rewards in db.Rewards on teacherreward.RewardId equals rewards.Id
                  where attestation.Name.Contains(cathegory)
                  select new SearchbyFIO
                  {
                    Name = teacher.Name,
                    Surname = teacher.Surname,
                    Second_name = teacher.Second_name,
                    Education = education.Name,
                    AttestationDate = attestation.Date,
                    CathegoryName = attestation.Name                             
                  }).ToList();*/
      var teachers = db.Teachers.Where(e => e.Attestation.Name.Contains(cathegory))
        .Select(s => new SearchbyFIO {
          Name = s.Name,
          Surname = s.Surname,
          Second_name = s.Second_name,
          Education = db.EducationTeachers.Where(e => e.TeacherId == s.Id).Select(r => r.Education.Name).FirstOrDefault(),
          AttestationDate = s.Attestation.Date,
          CathegoryName = s.Attestation.Name
        })
        .ToList();
      return Request.CreateResponse(HttpStatusCode.OK, teachers);
    }

    [HttpGet]
    [Route("searchbyreward")]
    public HttpResponseMessage SearchbyReward(int rewardId)
    {
      var teachers = db.Teachers.Where(e => e.TeacherRewards.Where(s => s.RewardId == rewardId).Any())
        .Select(s => s.Surname + " " + s.Name + " " + s.Second_name).ToList();
      return Request.CreateResponse(HttpStatusCode.OK, teachers);
    }

    private class SearchByFIOModel
    {
      public string FIO { get; set; }
      public string Education { get; set; }
      public string Attestation { get; set; }
      public string TeacherExperience { get; set; }
      public List<string> Rewards = new List<string>(1);
    }
  }
}
