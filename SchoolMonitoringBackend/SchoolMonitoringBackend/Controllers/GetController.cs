﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Areas;

namespace SchoolMonitoringBackend.Controllers
{
  [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
  public class GetController : ApiController
  {
    [HttpGet]
    [Route("getrewardlist")]
    public HttpResponseMessage GetRewardList()
    {
      using (var db = new SchoolContext())
      {
        var rewards = db.Rewards.Select(s => new
        {
          s.Id,
          s.Name
        }).ToList();
        return Request.CreateResponse(HttpStatusCode.OK, rewards);
      }
    }
  }
}
