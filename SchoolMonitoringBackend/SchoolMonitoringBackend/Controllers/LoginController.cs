﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Services;
using SchoolMonitoringBackend.Models.AuxilliaryModels;
using System.Data.Entity;
using SchoolMonitoringBackend.Areas;

namespace SchoolMonitoringBackend.Controllers
{
  [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
  public class LoginController : ApiController
  {
    SchoolContext db = new SchoolContext();

    [HttpGet]
    [Route("getuserbylogin")]
    public HttpResponseMessage GetUserByLogin(string login)
    {
      using (var db = new SchoolContext())
      {
        var user = db.Teachers.Where(e => e.DataUser.Login == login)
          .Select(s => new LoginData
          {
            TeacherId = s.Id,
            Login = s.DataUser.Login,
            Password = s.DataUser.Password,
            RoleName = s.DataUser.RoleName
          })
          .FirstOrDefault();
        Teacher teacher = db.Teachers.Find(user.TeacherId);
        teacher.LastVisit = DateTime.Now.AddHours(4);
        db.Entry(teacher).State = EntityState.Modified;
        db.SaveChanges();
        var response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
      }          
    }
    [HttpPost]
    [Route("createteacher")]
    public string CreateTeacher(LoginData user)
    {
      if (user.keyWord == "562113")
      {
        using (var db = new SchoolContext())
        {
          var existTeacher = db.DataUsers
            .Where(e => e.Login == user.Login)
            .FirstOrDefault();
          if (existTeacher == null)
          {
            DataUser newDataUser = new DataUser
            {
              Login = user.Login,
              Password = user.Password,
              RoleName = "2"
            };
            db.DataUsers.Add(newDataUser);
          }
          else
            return "Учетная запись с таким логином уже существует!";
        }
      }
      else
        return "Неправильно указано ключевое слово";

      return "Учетная запись создана!";
    }
  }
}
