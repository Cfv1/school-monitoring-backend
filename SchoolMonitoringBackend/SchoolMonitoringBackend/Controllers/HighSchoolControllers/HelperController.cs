﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.HighSchoolModels;
using System.Data.Entity;
using SchoolMonitoringBackend.Areas;

namespace SchoolMonitoringBackend.Controllers.HighSchoolControllers
{
    [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
    public class HelperController: ApiController
    {
        /// <summary>
        /// Получить список всех четвертей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getQuarters")]
        public IEnumerable<object> GetQuarters()
        {
            SchoolContext db = new SchoolContext();
            var quart = db.Quarters
                .Select(q => new
                {
                    Id = q.Id,
                    Name = q.Name,
                    DateStart = q.Date_of_start,
                    DateEnd = q.Date_of_ending
                })
                .Where(e => e.Name != null)
                .ToList();
            return quart;
        }

        /// <summary>
        /// Получить список всех годов (данные хранятся за последние пять лет)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getYearOfStudy")]
        public IEnumerable<object> GetYears()
        {
            SchoolContext db = new SchoolContext();
            var quart = db.YearOfStudies
                .Select(q => new
                {
                    Id = q.Id,
                    Name = q.Year,
                })
                .Where(e => e.Name != null)
                .ToList();
            return quart;
        }

        /// <summary>
        /// Получить список учителей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getTeachers")]
        public IEnumerable<object> GetTeachers()
        {
            SchoolContext db = new SchoolContext();
            var teachers = db.Teachers
                .Select(t => new
                {
                    t.Id,
                    t.Name,
                    t.Surname,
                    SecondName = t.Second_name,
                    t.Gender
                })
                .ToList();
            return teachers;
        }

        /// <summary>
        /// Получить информацию по определенному учителю
        /// </summary>
        /// <param name="teacher_Id">ID учителя</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getTeacher")]
        public IEnumerable<object> GetTeachers(int teacher_Id)
        {
            SchoolContext db = new SchoolContext();
            var teacher = db.Teachers
                .Where(id => id.Id == teacher_Id)
                .Select(t => new
                {
                    t.Id,
                    t.Name,
                    t.Surname,
                    SecondName = t.Second_name,
                    t.Gender
                })
                .ToList();
            return teacher;
        }

        /// <summary>
        /// Получить список классов, у которых определенный учитель является классным руководителем
        /// </summary>
        /// <param name="teacher_Id">ID учителя</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getClassesForTeacher")]
        public IEnumerable<object> GetClasses(int teacher_Id)
        {
            List<ClassInfo> ciL = new List<ClassInfo>();
            SchoolContext db = new SchoolContext();
            var classes = db.TeacherClasses
                .Where(t => t.TeacherId == teacher_Id)
                .Where(y => y.YearOfStudyId == 1) // поменять
                .Select(c => new
                {
                    Id = c.Class.Id,
                    Name = c.Class.Name,
                    PupilsCount = db.ClassPupils.Where(p => p.YearOfStudyId == 1) // поменять
                                                .Where(cl => cl.ClassId == c.Class.Id)
                                                .Count(),
                    TeacherName = c.Teacher.Surname + " " + c.Teacher.Name + " " + c.Teacher.Second_name
                })
                .Distinct()
                .ToList();


            return classes;
        }

        /// <summary>
        /// Получить список учеников по определенному классу
        /// </summary>
        /// <param name="class_Id">ID класса</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getPupilsForClass")]
        public IEnumerable<object> GetPupilsForClass(int class_Id)
        {
            SchoolContext db = new SchoolContext();
            var pupils = db.ClassPupils
                .Where(c => c.ClassId == class_Id)
                .Select(p => new
                {
                    p.Pupil.Id,
                    p.Pupil.Name,
                    p.Pupil.Surname,
                    SecondName = p.Pupil.Second_name,
                    p.Pupil.Gender,
                    DateOfBirth = p.Pupil.Date_of_birth,
                    ClassName = p.Class.Name
                })
                .ToList();
            return pupils;
        }
    }
}