﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.HighSchoolModels;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using SchoolMonitoringBackend.Areas;

namespace SchoolMonitoringBackend.Controllers.HighSchoolControllers
{

    [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
    public class SubjectController : ApiController
    {
        /// <summary>
        /// Получить отчет по определенному предмету по классу, который закреплен за учителем
        /// </summary>
        /// <param name="teacher_Id">ID учителя</param>
        /// <param name="subject_Id">ID предмета</param>
        /// <param name="quarter_Id">ID четверти</param>
        /// <param name="year_Id">ID года</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSubjectReport")]
        public List<ReportSubject> GetSubjectReport(int teacher_Id, int subject_Id, int quarter_Id, int year_Id)
        {
            SchoolContext db = new SchoolContext();
            List<ReportSubject> rsL = new List<ReportSubject>();

            // Учитель, который хочет посмотреть отчет
            var teacher = db.Teachers.Find(teacher_Id);

            // Список классов, в которых учитель ведет предмет
            var allClasses = db.TeacherSubjects
                .Where(t => t.TeacherId == teacher.Id)
                .Where(s => s.SubjectId == subject_Id)
                .Select(c => new
                {
                    Id = c.ClassId,
                    Name = c.Class.Name
                })
                .Distinct()
                .ToList();

            // Пройтись по списку классов и загрузить в свой список
            foreach (var c in allClasses)
            {
                ReportSubject rs = new ReportSubject();
                rs.ClassName = c.Name;
                rsL.Add(rs);
            }

            // Пройтись по своему списку и загрузить его всей информацией требуемой для отчета
            foreach (var r in rsL)
            {
                // Получить количество учеников в классах
                //var smpupil = db.Pupils
                //    .Where(c => c.Class.Name == r.ClassName).Count();

                var smpupil = db.ClassPupils.Where(p => p.YearOfStudyId == year_Id) // поменять
                                                .Where(cl => cl.Class.Name == r.ClassName)
                                                .Count();
                if (smpupil != 0)
                {
                    for (int eval = 2; eval <= 5; eval++)
                    {
                        // Посчитать количество оценок
                        var value = db.PupilGrades
                           .Where(s => s.SubjectId == subject_Id)
                           .Where(q => q.QuarterId == quarter_Id)
                           .Where(y => y.YearOfStudyId == year_Id)
                           .Where(c => c.Pupil.ClassPupils.Any(t => t.Class.Name == r.ClassName))
                           .Where(e => e.Evaluation == eval)
                           .Count();
                        if (eval == 2)
                            r.TwoCount = value;
                        if (eval == 3)
                            r.ThreeCount = value;
                        if (eval == 4)
                            r.FourCount = value;
                        if (eval == 5)
                            r.FiveCount = value;
                    }
                    r.PupilsCount = smpupil;
                    r.AbsoluteProgress = ((r.PupilsCount - r.TwoCount) / r.PupilsCount) * 100;
                    r.QualitiveProgress = ((r.PupilsCount - r.TwoCount - r.ThreeCount) / r.PupilsCount) * 100;
                    r.TeacherName = teacher.Surname + " " + teacher.Name + " " + teacher.Second_name;
                    r.QuarterName = db.Quarters.Find(quarter_Id).Name.ToString();
                    r.SubjectName = db.Subjects.FirstOrDefault(i => i.Id == subject_Id).Name;
                    r.Year = db.YearOfStudies.Find(year_Id).Year;
                }
            }
            return rsL;
        }

        /// <summary>
        /// Получить список предметов, которые преподает учитель
        /// </summary>
        /// <returns></returns>
       // [HttpGet]
        [Route("getSubjects")]
        public IEnumerable<object> GetSubjects()
        {
            SchoolContext db = new SchoolContext();
            var subj = db.Subjects
                .Select(sub => new
                {
                    Id = sub.Id,
                    Name = sub.Name,
                })
                .ToList();
            return subj;
        }

        /// <summary>
        /// Получить список учеников с четвертными оценками по определенному предмету
        /// </summary>
        /// <param name="subject_Id">ID предмета</param>
        /// <param name="class_Id">ID класса</param>
        /// <param name="year_Id">ID учебного года</param>
        /// <param name="quarter_Id">ID четверти</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getPupilsWithMarksForSubject")]
        public IEnumerable<object> GetPupilsWithMarksForSubject(int subject_Id, int class_Id, int year_Id, int quarter_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                List<PupilWithMark> pwmL = new List<PupilWithMark>();

                // Подсчитать всех учеников в классе
                var pupils = db.ClassPupils.Include(p => p.Pupil)
                    .Where(c => c.ClassId == class_Id)
                    .ToList()
                    .Distinct();

                // Заполнить свой список данными
                foreach (var p in pupils)
                {
                    PupilWithMark pwm = new PupilWithMark();
                    pwm.PupilId = p.PupilId;
                    pwm.PupilName = p.Pupil.Name;
                    pwm.PupilSurname = p.Pupil.Surname;
                    var value = db.PupilGrades
                        .Where(s => s.SubjectId == subject_Id)
                        .Where(c => c.PupilId == p.PupilId)
                        .Where(q => q.QuarterId == quarter_Id)
                        .FirstOrDefault(y => y.YearOfStudyId == year_Id);
                    if (value == null)
                        pwm.Mark = 0;
                    else
                        pwm.Mark = value.Evaluation;
                    pwmL.Add(pwm);
                }
                return pwmL;
            }
        }
        
        /// <summary>
        /// Добавить или редактировать четвертные оценки учеников по определенному предмету
        /// </summary>
        /// <param name="pupils">Список учеников с результатами</param>
        /// <param name="year_Id">ID учебного года</param>
        /// <param name="quarter_Id">ID четверти</param>
        /// <param name="subject_Id">ID предмета</param>
        [HttpPost]
        [Route("addSubjectMarksForPupils")]
        public void AddSubjectMarksForPupils([FromBody] PupilWithMark[] pupils, int year_Id, int quarter_Id, int subject_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                foreach (var p in pupils)
                {
                    var subject = db.PupilGrades
                        .Where(y => y.YearOfStudyId == year_Id)
                        .Where(s => s.SubjectId == subject_Id)
                        .Where(q => q.QuarterId == quarter_Id)
                        .FirstOrDefault(pi => pi.PupilId == p.PupilId);

                    if (subject != null)
                        subject.Evaluation = p.Mark;
                    else
                    {
                        PupilGrade pg = new PupilGrade();
                        pg.PupilId = p.PupilId;
                        pg.SubjectId = subject_Id;
                        pg.YearOfStudyId = year_Id;
                        pg.QuarterId = quarter_Id;
                        pg.Evaluation = p.Mark;
                        db.PupilGrades.Add(pg);
                    }
                }
                //  db.SaveChanges();
            }
        }
    }
}