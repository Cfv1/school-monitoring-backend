﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.HighSchoolModels;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using SchoolMonitoringBackend.Areas;

namespace SchoolMonitoringBackend.Controllers.HighSchoolControllers
{
    [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
    public class EventController : ApiController
    {
        /// <summary>
        /// Получить список событий с общей информацией о каждом
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getInfoEvents")]
        public IEnumerable<object> GetAllEvents()
        {
            SchoolContext db = new SchoolContext();
            var events = db.EventForClevers
                .Select(e => new
                {
                    e.Id,
                    e.Name,
                    LevelName = e.EventLevel.Name,
                    TypeName = e.EventType.Name
                })
                .ToList();
            return events;
        }

        /// <summary>
        /// Получить список событий по определенной выборке.
        /// </summary>
        /// <param name="level_Id">ID уровня события</param>
        /// <param name="month_num">Номер месяца, когда произошло событие</param>
        /// <param name="year_Id">ID учебного года, когда произошло событие</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getEvents")]
        public IEnumerable<object> GetEvents(int level_Id, int month_num, int year_Id)
        {
            SchoolContext db = new SchoolContext();
            if (month_num == 0)
            {
                var events = db.EventForClevers
                .Where(l => l.EventLevelId == level_Id)
                .Where(y => y.YearOfStudyId == year_Id)
                .Select(e => new
                {
                    Id = e.Id,
                    Name = e.Name,
                    LevelName = e.EventLevel.Name,
                    TypeName = e.EventType.Name,
                    MembersCount = db.EventPupils.Where(m => m.EventForCleverId == e.Id).Count(),
                    Year = e.YearOfStudy.Year,
                    Month = e.Month,
                    Quarter = e.Quarter.Name
                })
                .ToList();
                return events;
            }
            else
            {
                var events = db.EventForClevers
                .Where(l => l.EventLevelId == level_Id)
                .Where(y => y.YearOfStudyId == year_Id)
                .Where(m=>m.Month == month_num)
                .Select(e => new
                {
                    Id = e.Id,
                    Name = e.Name,
                    LevelName = e.EventLevel.Name,
                    TypeName = e.EventType.Name,
                    MembersCount = db.EventPupils.Where(m => m.EventForCleverId == e.Id).Count(),
                    Year = e.YearOfStudy.Year,
                    Month = e.Month,
                    Quarter = e.Quarter.Name
                })
                .ToList();
                return events;
            }
        }

        /// <summary>
        /// Получить информацию по определенному событию
        /// </summary>
        /// <param name="event_Id">ID события</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getEvent")]
        public IEnumerable<object> GetEvent(int event_Id)
        {
            SchoolContext db = new SchoolContext();
            var events = db.EventForClevers
                .Where(e => e.Id == event_Id)
                .Select(e => new
                {
                    Id = e.Id,
                    Name = e.Name,
                    LevelName = e.EventLevel.Name,
                    TypeName = e.EventType.Name,
                    MembersCount = db.EventPupils.Where(m => m.EventForCleverId == e.Id).Count(),
                    Year = e.YearOfStudy.Year,
                    Quarter = e.Quarter.Name
                })
                .ToList();

            return events;
        }

        /// <summary>
        /// Получить отчет по определенному событию
        /// </summary>
        /// <param name="event_Id">ID события</param>
        /// <returns></returns>
        /*[HttpGet]
        [Route("getSpecEvent")]
        public IEnumerable<object> GetSpecEvent(int event_Id)
        {
            SchoolContext db = new SchoolContext();
            List<ReportEvent> reL = new List<ReportEvent>();
            int nowYear = DateTime.Now.Year;
            var eventYear = db.EventForClevers.Find(event_Id);
            var eventForClever = db.EventPupils
                .Include(t=>t.Teacher.Experience.Date_of_start)
                .Where(e => e.EventForCleverId == event_Id)
                .Select(p => new
                {
                    PupilId = p.Pupil.Id,
                    PupilName = p.Pupil.Name,
                    PupilSurname = p.Pupil.Surname,
                    PupilClass = p.Pupil.ClassPupils.Where(e=>e.YearOfStudyId == eventYear.YearOfStudyId)
                                                    .FirstOrDefault(pup=>pup.PupilId == p.PupilId).Class.Name,
                    PupilPlace = p.Place,
                    TeacherName = p.Teacher.Surname + " " + p.Teacher.Name + " " + p.Teacher.Second_name,
                    TeacherExperience = nowYear - p.Teacher.Experience.Date_of_start.Value.Year
                })
                .ToList();

            return eventForClever;
        }*/


        /// <summary>
        /// Получить список уровней события
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventLevels")]
        public IEnumerable<object> GetLevels()
        {
            SchoolContext db = new SchoolContext();
            var levels = db.EventLevels
                .Select(c => new
                {
                    c.Id,
                    c.Name
                })
                .ToList();
            return levels;
        }

        /// <summary>
        /// Получить список типов событий
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getEventTypes")]
        public IEnumerable<object> GetEventTypes()
        {
            SchoolContext db = new SchoolContext();
            var types = db.EventTypes
                .Select(c => new
                {
                    c.Id,
                    c.Name
                })
                .ToList();
            return types;
        }

        /// <summary>
        /// Добавить событие в базу данных
        /// </summary>
        /// <param name="eventForClever">Модель мероприятия</param>
        [HttpPost]
        [Route("addEvent")]
        public void CreateEvent([FromBody] EventForClever eventForClever)
        {
            SchoolContext db = new SchoolContext();
            db.EventForClevers.Add(eventForClever);
            //db.SaveChanges();
           
        }

        /// <summary>
        /// Записать учеников на определенное событие
        /// </summary>
        /// <param name="pupils">Список учеников</param>
        /// <param name="teacher_Id">ID ментора</param>
        /// <param name="event_Id">ID события</param>
        [HttpPost]
        [Route("addPupilsForEvent")]
        public void PostPupilsForEvent([FromBody] Pupil[] pupils, int teacher_Id, int event_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                foreach (var p in pupils)
                {
                    EventPupil ep = new EventPupil();
                    ep.PupilId = p.Id;
                    ep.TeacherId = teacher_Id;
                    ep.EventForCleverId = event_Id;
                }
                //  db.SaveChanges();
            }
        }

        /// <summary>
        /// Получить список учеников с их результатами по событию (результаты возможно пустые)
        /// </summary>
        /// <param name="event_Id">ID события</param>
        /// <param name="class_Id">ID класса</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getPupilsWithPlaces")]
        public IEnumerable<object> GetPupilsWithMarks(int event_Id, int class_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                var eventYear = db.EventForClevers.Find(event_Id);
                var pupilsWithMarks = db.EventPupils
                .Where(e => e.EventForCleverId == event_Id)
                .Select(p => new
                {
                    PupilId = p.PupilId,
                    PupilName = p.Pupil.Name,
                    PupilSurname = p.Pupil.Surname,
                    Place = p.Place,
                    ClassId = p.Pupil.ClassPupils.Where(e => e.YearOfStudyId == eventYear.YearOfStudyId)
                                                    .FirstOrDefault(pup => pup.PupilId == p.PupilId).Class.Id,
                })
                .Where(g=>g.ClassId == class_Id)
                .ToList();
                return pupilsWithMarks;
            }
        }

        /// <summary>
        /// Добавить к имеющимся записанным участникам события некоторые оценки или результаты
        /// </summary>
        /// <param name="pupils">Список учеников с результатами</param>
        /// <param name="event_Id">ID события</param>
        [HttpPost]
        [Route("addPlacesForPupils")]
        public void AddPlacesForPupils([FromBody] PupilWithPlace[] pupils, int event_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                foreach (var p in pupils)
                {
                    var pup = db.EventPupils
                        .FirstOrDefault(c => c.EventForCleverId == event_Id && c.PupilId == p.PupilId);
                    pup.Place = p.Place;
                }
                //  db.SaveChanges();
            }
        }
    }
}