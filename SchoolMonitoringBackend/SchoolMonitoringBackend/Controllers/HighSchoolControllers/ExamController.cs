﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.HighSchoolModels;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using SchoolMonitoringBackend.Areas;

namespace SchoolMonitoringBackend.Controllers.HighSchoolControllers
{
    [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
    public class ExamController : ApiController
    {
        /// <summary>
        /// Получить информацию о классах
        /// </summary>
        /// <param name = "className" > Цифра класса(4, 9 или 11)</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getClassesForReport")]
        public IEnumerable<object> GetClassesForReport(string className)
        {
            using (SchoolContext db = new SchoolContext())
            {
                List<ClassInfo> ciL = new List<ClassInfo>();

                // Список классов, название которого содержит в себе className (4, 9 или 11)
                var classes = db.Classes
                    .Where(n => n.Name.Contains(className.Trim()))
                    .ToList();

                // Пройтись по своему списку и заполнить его необходимыми данными
                classes.ForEach(c => 
                {
                    ClassInfo ci = new ClassInfo();
                    ci.Id = c.Id;
                    ci.Name = c.Name;
                    ci.PupilsCount = db.ClassPupils.Where(o => o.ClassId == c.Id)
                        .Count();
                    ci.TeacherName = db.TeacherClasses
                        .Include(t => t.Teacher)
                        .Where(y => y.YearOfStudyId == 1)
                        .FirstOrDefault(t => t.ClassId == c.Id).Teacher.Surname + " " +
                        db.TeacherClasses
                        .Include(t => t.Teacher)
                        .Where(y => y.YearOfStudyId == 1)
                        .FirstOrDefault(t => t.ClassId == c.Id).Teacher.Name + " " +
                        db.TeacherClasses
                        .Include(t => t.Teacher)
                        .Where(y => y.YearOfStudyId == 1)
                        .FirstOrDefault(t => t.ClassId == c.Id).Teacher.Second_name;
                    ciL.Add(ci);
                });

                return ciL;
            }

        }

        /// <summary>
        /// Получить отчет по экзамену за 4 или 9 класс(ВПР или ОГЭ)
        /// </summary>
        /// <param name = "class_Id" > ID класса</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getNineClassReport")]
        public IEnumerable<object> GetNineClassReport(int class_Id, int year_Id)
        {
            SchoolContext db = new SchoolContext();
            List<ReportNineExam> rneL = new List<ReportNineExam>();

            //Узнать идентификатор типа сдаваемого экзамена (1(ОГЭ), 2(ЕГЭ) или 3(ВПР)), зная наименование класса
            int type_Id = whatTypeIdForThis(class_Id);

            // Количество всех учеников в классе
            int pupilsCount = howManyPupilsAreInThis(class_Id, year_Id).Count();

            // Список экзаменов, которые они сдавали
            List<string> subjectL = whatExamsWereInThis(class_Id);

            // Заполнение списка этими предметами
            subjectL.ForEach(s =>
            {
                ReportNineExam rne = new ReportNineExam();
                rne.SubjectName = s;
                rneL.Add(rne);
            });

            // Пройтись по списку с предметами и заполнить принадлежность всех данных с этим предметом
            rneL.ForEach(n =>
            {
                // список всех учеников, сдававшие этот предмет на экзамене
                List<Exam> pupilMemberL = membersForThisSubject(n.SubjectName, type_Id, class_Id, year_Id);

                n.PupilsCount = pupilsCount;
                n.MemberPupilsCount = pupilMemberL.Count();
                n.MemberPupilsPercent = ((n.MemberPupilsCount * 100) / pupilsCount).ToString();
                for (int eval = 2; eval <= 5; eval++)
                {
                    var evaluation = pupilMemberL.Where(f => f.Evaluation == eval).Count();
                    if (eval == 2)
                    {
                        n.TwoCount = evaluation;
                        n.TwoPercent = ((n.TwoCount * 100) / pupilsCount).ToString();
                    }
                    if (eval == 3)
                    {
                        n.ThreeCount = evaluation;
                        n.ThreePercent = ((n.ThreeCount * 100) / pupilsCount).ToString();
                    }
                    if (eval == 4)
                    {
                        n.FourCount = evaluation;
                        n.FourPercent = ((n.FourCount * 100) / pupilsCount).ToString();
                    }
                    if (eval == 5)
                    {
                        n.FiveCount = evaluation;
                        n.FivePercent = ((n.FiveCount * 100) / pupilsCount).ToString();
                    }
                }
                n.AbsolutePercent = (((n.PupilsCount - n.TwoCount) / n.PupilsCount) * 100).ToString();
                n.QualitivePercent = (((n.PupilsCount - n.TwoCount - n.ThreeCount) / n.PupilsCount) * 100).ToString();
                n.ClassName = db.Classes.Find(class_Id).Name;
                var teacher = db.TeacherClasses
                    .Include(t => t.Teacher)
                    .Where(y => y.YearOfStudyId == 1)
                    .FirstOrDefault(t => t.ClassId == class_Id).Teacher;
                n.TeacherName = teacher.Surname + " " + teacher.Name + " " + teacher.Second_name;
                n.Year = db.YearOfStudies.Find(year_Id).Year;
            });
            return rneL;

        }

        /// <summary>
        /// Получить отчет по экзамену за 11 класс(ЕГЭ)
        /// </summary>
        /// <param name = "class_Id" > ID класса</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getElevenClassReport")]
        public IEnumerable<object> GetElevenClassReport(int class_Id, int year_Id)
        {
            SchoolContext db = new SchoolContext();
            List<ReportElevenExam> reeL = new List<ReportElevenExam>();

            //Узнать идентификатор типа сдаваемого экзамена, зная наименование класса
            int type_Id = whatTypeIdForThis(class_Id);

            // Количество всех учеников в классе
            int pupilsCount = howManyPupilsAreInThis(class_Id, year_Id).Count();

            // Список экзаменов которые они сдавали
            List<string> subjectL = whatExamsWereInThis(class_Id);

            // Заполнение списка этими предметами
            subjectL.ForEach(s =>
            {
                ReportElevenExam ree = new ReportElevenExam();
                ree.SubjectName = s;
                reeL.Add(ree);
            });

            // Пройтись по списку с предметами и заполнить принадлежность всех данных с этим предметом
            reeL.ForEach(n =>
            {
                // список всех учеников, сдававшие этот предмет на экзамене
                List<Exam> pupilMemberL = membersForThisSubject(n.SubjectName, type_Id, class_Id, year_Id);

                n.Year = db.YearOfStudies.Find(year_Id).Year;
                n.PupilsCount = pupilsCount;
                n.MemberPupilsCount = pupilMemberL.Count();
                n.MemberPupilsPercent = ((n.MemberPupilsCount * 100) / pupilsCount).ToString();

                // Выгрузка количества учеников по баллам
                n.ToMinimumCount = pupilMemberL.Where(t => t.Threshold < t.ThresholdMin).Count();
                n.ToMininmumPercent = ((n.ToMinimumCount * 100) / pupilMemberL.Count).ToString();

                n.ToFiftyCount = pupilMemberL.Where(t => t.Threshold > t.ThresholdMin && t.Threshold <= 50).Count();
                n.ToFiftyPercent = ((n.ToFiftyCount * 100) / pupilMemberL.Count).ToString();

                for (int i = 50; i < 90; i += 10)
                {
                    var threshold = pupilMemberL.Where(t => t.Threshold > i && t.Threshold <= i + 10).Count();
                    switch (i)
                    {
                        case 50:
                            n.ToSixtyCount = threshold;
                            n.ToSixtyPercent = ((n.ToSixtyCount * 100) / pupilMemberL.Count).ToString();
                            break;
                        case 60:
                            n.ToSeventyCount = threshold;
                            n.ToSeventyPercent = ((n.ToSeventyCount * 100) / pupilMemberL.Count).ToString();
                            break;
                        case 70:
                            n.ToEightyCount = threshold;
                            n.ToEightyPercent = ((n.ToEightyCount * 100) / pupilMemberL.Count).ToString();
                            break;
                        case 80:
                            n.ToNintyCount = threshold;
                            n.ToNintyPercent = ((n.ToNintyCount * 100) / pupilMemberL.Count).ToString();
                            break;
                        default:
                            break;
                    }
                }
                n.ToHundredCount = pupilMemberL.Where(t => t.Threshold > 90 && t.Threshold <= 99).Count();
                n.ToHundredPercent = ((n.ToHundredCount * 100) / pupilMemberL.Count).ToString();

                n.HundredCount = pupilMemberL.Where(t => t.Threshold == 100).Count();
                n.HundredPercent = ((n.HundredCount * 100) / pupilMemberL.Count).ToString();

                n.ClassName = db.Classes.Find(class_Id).Name;
                var teacher = db.TeacherClasses
                    .Include(t => t.Teacher)
                    .Where(y => y.YearOfStudyId == 1)
                    .FirstOrDefault(t => t.ClassId == class_Id).Teacher;
                n.TeacherName = teacher.Surname + " " + teacher.Name + " " + teacher.Second_name;
            });
            return reeL;

        }

        /// <summary>
        /// Получить список учеников с оценками за экзамен по определенному предмету
        /// </summary>
        /// <param name="subject_Id">ID предмета</param>
        /// <param name="class_Id">ID класса</param>
        /// <param name="year_Id">ID учебного года</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getPupilsWithMarksForExam")]
        public IEnumerable<object> GetPupilsWithMarksForExam(int subject_Id, int class_Id, int year_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                List<PupilWithMark> pwmL = new List<PupilWithMark>();

                // Подсчитать всех учеников в классе
                var pupils = howManyPupilsAreInThis(class_Id, year_Id);

                // Заполнить свой список данными
                pupils.ForEach(p =>
                {
                    PupilWithMark pwm = new PupilWithMark();
                    pwm.PupilId = p.PupilId;
                    pwm.PupilName = p.Pupil.Name;
                    pwm.PupilSurname = p.Pupil.Surname;
                    var value = db.Exams
                        .Where(s => s.SubjectId == subject_Id)
                        .Where(c => c.PupilId == p.PupilId)
                        .FirstOrDefault(y => y.YearOfStudyId == year_Id);
                    if (value == null || value.Evaluation == null)
                        pwm.Mark = 0;
                    else
                        pwm.Mark = value.Evaluation.Value;
                    pwmL.Add(pwm);
                });
                return pwmL;
            }
        }

        /// <summary>
        /// Добавить или редактировать экзаменационные оценки учеников
        /// </summary>
        /// <param name="pupils">Список учеников с результатами</param>
        /// <param name="year_Id">ID учебного года</param>
        /// <param name="subject_Id">ID предмета</param>
        /// <param name="class_Id">ID класса</param>
        [HttpPost]
        [Route("addExamMarksForPupils")]
        public void AddExamMarksForPupils([FromBody] PupilWithMark[] pupils, int year_Id, int subject_Id, int class_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                // тип экзамена
                string examType = db.Classes.Find(class_Id).Name;
                int classType = Int32.Parse(examType.Substring(0, examType.Length - 1));

                foreach (var p in pupils)
                {
                    var exam = db.Exams
                        .Where(y => y.YearOfStudyId == year_Id)
                        .Where(s => s.SubjectId == subject_Id)
                        .FirstOrDefault(pi => pi.PupilId == p.PupilId);

                    if (exam == null)
                    {
                        Exam e = new Exam();
                        e.PupilId = p.PupilId;
                        e.SubjectId = subject_Id;
                        e.YearOfStudyId = year_Id;
                        if (classType == 9)
                        {
                            e.ExamTypeId = 1;
                            e.Evaluation = p.Mark;
                        }
                        if (classType == 4)
                        {
                            e.ExamTypeId = 3;
                            e.Evaluation = p.Mark;
                        }
                        if (classType == 11)
                        {
                            e.ExamTypeId = 2;
                            e.Threshold = p.Mark;
                        }
                        db.Exams.Add(e);
                    }
                    else
                    {
                        if (classType == 9 || classType == 4)
                            exam.Evaluation = p.Mark;
                        if (classType == 11)
                            exam.Threshold = p.Mark;
                    }
                }
                //  db.SaveChanges();
            }
        }

        /// <summary>
        /// Метод-хелпер. Получить тип сдаваемого экзамена по классу
        /// </summary>
        /// <param name="class_Id">ID класса</param>
        /// <returns></returns>
        private int whatTypeIdForThis(int class_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                int typeId;
                string fullClassName = db.Classes.Find(class_Id).Name;
                int numberClassName = Int32.Parse(fullClassName.Substring(0, fullClassName.Length - 1));
                switch (numberClassName)
                {
                    case 4:
                        typeId = 3;
                        break;
                    case 9:
                        typeId = 1;
                        break;
                    case 11:
                        typeId = 2;
                        break;
                    default:
                        typeId = 0;
                        break;
                }
                return typeId;
            }
        }

        /// <summary>
        /// Метод-хелпер. Получить список учеников учащихся в определенном классе
        /// </summary>
        /// <param name="class_Id">ID класса</param>
        /// <param name="year_Id">ID учебного года</param>
        /// <returns></returns>
        private List<ClassPupil> howManyPupilsAreInThis(int class_Id, int year_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                var allPupilsInTheClass = db.ClassPupils
                    .Include(p => p.Pupil)
                    .Where(c => c.ClassId == class_Id)
                    .Where(y => y.YearOfStudyId == year_Id)
                    .ToList();
                return allPupilsInTheClass;
            }
        }

        /// <summary>
        /// Метод-хелпер. Получить список экзаменов, которые сдавали в определенном классе
        /// </summary>
        /// <param name="class_Id">ID класса</param>
        /// <returns>Список экзаменов</returns>
        private List<string> whatExamsWereInThis(int class_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                List<string> subjectL = new List<string>();
                var subject = db.Exams.Where(c => c.Pupil.ClassPupils.Any(t => t.ClassId == class_Id))
                    .Select(s => new { Name = s.Subject.Name })
                    .GroupBy(s => s.Name)
                    .ToList();
                subject.ForEach(s => subjectL.Add(s.Key));
                return subjectL;
            }
        }

        /// <summary>
        /// Метод-хелпер. Получить список учеников сдававших предмет на экзамене по некоторым параметрам
        /// </summary>
        /// <param name="subjectName">Наименование предмета</param>
        /// <param name="type_Id">ID типа экзамена (1 (ОГЭ), 2 (ЕГЭ) или 3 (ВПР))</param>
        /// <param name="class_Id">ID класса</param>
        /// <param name="year_Id">ID учебного года</param>
        /// <returns>Список учеников</returns>
        private List<Exam> membersForThisSubject(string subjectName, int type_Id, int class_Id, int year_Id)
        {
            using (SchoolContext db = new SchoolContext())
            {
                var pupilMemberCountL = db.Exams
                       .Where(p => p.Subject.Name == subjectName)
                       .Where(t => t.ExamTypeId == type_Id)
                       .Where(p => p.Pupil.ClassPupils.Any(t => t.ClassId == class_Id))
                       .Where(y => y.YearOfStudyId == year_Id)
                       .ToList();
                return pupilMemberCountL;
            }
        }
    }
}