﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Web.Http.Cors;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.AuxilliaryModels;
using SchoolMonitoringBackend.Services;
using SchoolMonitoringBackend.Models.ViewModels;
using SchoolMonitoringBackend.Areas;

namespace SchoolMonitoringBackend.Controllers
{
  [EnableCors(origins: GeneralHost.URL, headers: "*", methods: "*")]
  public class TeacherPanelController : ApiController
  {
    SchoolContext db = new SchoolContext();

    [HttpPost]
    [Route("editeducation")]
    public HttpResponseMessage EditEducation(AddEducationViewModel postModel)
    {
      var created = db.EducationTeachers.Where(e => e.TeacherId == postModel.TeacherId)
        .FirstOrDefault();      
      EducationTeacher newEducation = new EducationTeacher
      {
        TeacherId = postModel.TeacherId,
        EducationId = postModel.educationId,
        Year_of_graduating = postModel.date,
        Institution = postModel.whatGraduated,
        DiplomSpecialty = postModel.specialty
      };
      if (created != null)
      {
        db.EducationTeachers.Remove(created);
      }
        db.EducationTeachers.Add(newEducation);
        db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.Created);
    }

    //переработать для непоказа выбывших
    [HttpGet]
    [Route("getpupils")]
    public HttpResponseMessage GetPupils(int TeacherId)
    {
      int classId = db.TeacherClasses.Where(t => t.TeacherId == TeacherId)
        .Select(s => s.ClassId).FirstOrDefault();
      if (classId == 0)
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Вы не являетесь классным руководителем");
      var pupils = db.Pupils.Where(p => p.ClassPupils.Any(c => c.Class.Id == classId))
      .Select(s => new
      {
        s.Id,
        s.Surname,
        s.Name,
        SecondName = s.Second_name,
        s.Gender,
        s.Date_of_birth,
        ArrivedDate = s.Arrived.Date_of_arrival,
        RetiredDate = s.Retired.Date_of_retirement,
        s.HardFamily,
        s.PartialFamily,
        s.SecondYear
      }).ToList();
      var sortedPupils = pupils.OrderBy(t => t.Surname).ToList();
      var response = Request.CreateResponse(HttpStatusCode.OK, sortedPupils);
      return response;
    }

    [HttpGet]
    [Route("getpupil")]
    public HttpResponseMessage GetPupil(int id)
    {
      var pupil = db.Pupils.Where(s => s.Id == id).Select(s=> new {
        s.Id,
        s.Surname,
        s.Name,
        SecondName = s.Second_name,
        s.Gender,
        s.Date_of_birth,
        ArrivedDate = s.Arrived.Date_of_arrival,
        s.Arrived.Place
      }).Single();
      return Request.CreateResponse(HttpStatusCode.OK, pupil);
    }

    //пофиксить для прибывших летом, а также проверка по фамилии имени отчеству
    [HttpPost]
    [Route("addpupil")]
    public HttpResponseMessage AddPupilInClass(AddPupilViewModel pupil)
    {
      var quarterAndYearId = db.Quarters.Where(q => pupil.ArrivedDate >= q.Date_of_start
        && pupil.ArrivedDate < q.Date_of_ending)
          .Select(s => new { QuarterId = s.Id, YearId = s.YearOfStudyId }).FirstOrDefault();
      if(quarterAndYearId == null)
      {
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, 
          "Укажите корректную дату поступления");
      }
      Arrived newArrived = new Arrived
      {
        Place = pupil.Place,
        QuarterId = quarterAndYearId.QuarterId,
        Date_of_arrival = pupil.ArrivedDate
      };
      db.Arriveds.Add(newArrived);
      Pupil newPupil = new Pupil
      {
        Name = pupil.Name,
        Second_name = pupil.Second_name,
        Surname = pupil.Surname,
        Gender = pupil.Gender,
        Date_of_birth = pupil.Date_of_birth
      };
      newPupil.Arrived = newArrived;
      db.Pupils.Add(newPupil);

      int classId;
      //для добавления учеников, которые уже учились раньше
      if (pupil.ArrivedDate > new DateTime(2018, 8, 31))
      {
        classId = db.TeacherClasses.Where(t => t.YearOfStudyId == quarterAndYearId.YearId
          && t.TeacherId == pupil.TeacherId)
          .Select(s => s.ClassId).First();
      }
      else
      {
        int currentYear = db.YearOfStudies.OrderByDescending(e=> e.Id)
          .Select(s => s.Id)
          .First();
        classId = db.TeacherClasses.Where(t => t.YearOfStudyId == currentYear
          && t.TeacherId == pupil.TeacherId)
          .Select(s => s.ClassId).First();
      }

      db.ClassPupils.Add(new ClassPupil
      {
        YearOfStudyId = quarterAndYearId.YearId,
        ClassId = classId,
        PupilId = newPupil.Id
      });
      db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.Created);
    }

    //подправить quarterID
    [HttpPost]
    [Route("editpupil")]
    public HttpResponseMessage EditPupil(AddPupilViewModel postModel)
    {
      Pupil pupil = db.Pupils.Find(postModel.Id);
      pupil.Name = postModel.Name;
      pupil.Surname = postModel.Surname;
      pupil.Second_name = postModel.Second_name;
      pupil.Gender = postModel.Gender;
      pupil.Date_of_birth = postModel.Date_of_birth;

      Arrived arrivedData = db.Arriveds.Find(pupil.ArrivedId);
      arrivedData.Date_of_arrival = postModel.ArrivedDate;
      arrivedData.Place = postModel.Place;
      db.Entry(pupil).State = EntityState.Modified;
      db.Entry(arrivedData).State = EntityState.Modified;
      db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.OK);
    }

    [HttpPost]
    [Route("addexperience")]
    public HttpResponseMessage EditExperience(AddExperienceViewModel experienceView)
    {
      Experience newExperience = new Experience
      {
        Date_of_start = experienceView.Date_of_start,
        Date_of_leaving = experienceView.Date_of_leaving
      };
      db.Experiences.Add(newExperience);
      db.TeacherExperiences.Add(new TeacherExperience
      {
        TeacherId = experienceView.TeacherId,
        ExperienceId = newExperience.Id,
        PedExperience = experienceView.PedExperience
      });
      db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.Created);
    }

    [HttpPost]
    [Route("addreward")]
    public HttpResponseMessage EditReward(AddRewardViewModel rewardView)
    {
      db.TeacherRewards.Add(new TeacherReward
      {
        TeacherId = rewardView.TeacherId,
        RewardId = rewardView.RewardId
      });
      db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.Created);
    }

    [HttpPost]
    [Route("editattestation")]
    public HttpResponseMessage EditAttestaion(EditAttestationViewModel postAttestation)
    {
      Teacher teacher = db.Teachers.First(f=> f.Id == postAttestation.TeacherId);
      if (teacher.AttestationId != null)
      {
        var oldAttestation = db.Attestations.Find(teacher.AttestationId);
        oldAttestation.Name = postAttestation.Name;
        oldAttestation.Place = postAttestation.Place;
        oldAttestation.Date = postAttestation.Date;
        oldAttestation.ActNumber = postAttestation.ActNumber;
        db.Entry(oldAttestation).State = EntityState.Modified;
      }
      else
      {
        Attestation newAttestation = new Attestation
        {
          Name = postAttestation.Name,
          Place = postAttestation.Place,
          Date = postAttestation.Date,
          ActNumber = postAttestation.ActNumber
        };
        db.Attestations.Add(newAttestation);
        teacher.AttestationId = newAttestation.Id;
      }
      db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.OK);
    }

    [HttpPost]
    [Route("adddissexperience")]
    public HttpResponseMessage AddDissExperience (AddDissExperienceViewModel postModel)
    {
      DisseminationExperience newDissExperience = new DisseminationExperience
      {
        Type_of_event = postModel.type,
        Level_of_event = postModel.level,
        Name_of_event = postModel.name,
        Form_of_view = postModel.formofView,
        Theme_of_speech = postModel.themeSpeech,
        Date = postModel.date
      };
      db.DisseminationExperiences.Add(newDissExperience);
      db.SaveChanges();
      DisseminationExperienceTeacher many_to_many = new DisseminationExperienceTeacher
      {
        TeacherId = postModel.TeacherId,
        DissExperienceId = newDissExperience.Id
      };
      db.DisseminationExperienceTeachers.Add(many_to_many);
      db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.Created);
    }
    
    [HttpPost]
    [Route("addqualification")]
    public HttpResponseMessage AddQualification(AddQualificationViewModel postModel)
    {
      Qualification newQualification = new Qualification
      {
        Count_of_hours = postModel.CountOfHours,
        Course_theme = postModel.CourseTheme,
        Date_of_leaving = postModel.DateOfEnding,
        Date_of_start = postModel.DateOfStart,
        Number_of_certification = postModel.NumberOfCertification,
        Organisation_name = postModel.OrganisationName,
        Studiyng_model = postModel.StudyingModel
      };
      db.Qualifications.Add(newQualification);
      db.SaveChanges();
      TeacherQualification many_to_many = new TeacherQualification
      {
        TeacherId = postModel.TeacherId,
        QualificationId = newQualification.Id
      };
      db.TeacherQualifications.Add(many_to_many);
      db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.Created);
    }

    [HttpPost]
    [Route("changeclass")]
    public HttpResponseMessage ChangeClass(ChangeClassViewModel postModel)
    {
      var teacherClass = db.TeacherClasses
        .Where(e=> e.Class.Name == postModel.ClassName && e.TeacherId != postModel.TeacherId)
        .SingleOrDefault();
      if (teacherClass != null)
      {
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Указанный класс уже имеет классного руководителя");
      }
      else if (teacherClass.TeacherId == postModel.TeacherId)
      {
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, $"Вы уже классный руководитель {teacherClass.Class.Name} класса!");
      }
      else
      {
        TeacherClass newTeacherClass = new TeacherClass
        {
          TeacherId = postModel.TeacherId,
          ClassId = db.Classes.Where(e => e.Name == postModel.ClassName)
          .Select(s => s.Id).First(),
          YearOfStudyId = db.YearOfStudies.Select(s => s.Id).ToList().Last(),
          Cabinet = postModel.Cabinet
        };
        db.TeacherClasses.Add(newTeacherClass);
        db.SaveChanges();
        return Request.CreateResponse(HttpStatusCode.Created);
      }
    }

    [HttpPost]
    [Route("addpublication")]
    public HttpResponseMessage AddPublication(AddPublicationViewModel postModel)
    {
      Publication publication = new Publication
      {
        Author = postModel.author,
        ArticleName = postModel.articleName,
        Publishing = postModel.publication,
        Year = postModel.year,
        UrlLink = postModel.link,
        Level = postModel.level
      };
      db.Publications.Add(publication);
      db.SaveChanges();
      TeacherPublication many_to_many = new TeacherPublication
      {
        PublicationId = publication.Id,
        TeacherId = postModel.TeacherId
      };
      db.TeacherPublications.Add(many_to_many);
      db.SaveChanges();
      return Request.CreateResponse(HttpStatusCode.Created);
    }

    [HttpPost]
    [Route("editsubject")]
    public HttpResponseMessage EditSubject(EditSubjectViewModel postModel)
    {
      var currentYear = DateTime.Now.Year;
      string currentStudyYear = currentYear.ToString() + "/" + (DateTime.Now.Year + 1).ToString();
      TeacherSubject teacherSubject = db.TeacherSubjects
        .Where(e => e.SubjectId == postModel.SubjectId &&
        e.Class.Name == postModel.ClassName.ToUpper() && 
        e.TeacherId == postModel.TeacherId && 
        e.YearOfStudy.Year == currentStudyYear)
        .FirstOrDefault();
      if (teacherSubject == null)
      {
        TeacherSubject newTeacherSubject = new TeacherSubject
        {
          TeacherId = postModel.TeacherId,
          SubjectId = postModel.SubjectId,
          ClassId = db.Classes.Where(e => e.Name == postModel.ClassName.ToUpper()).Select(s => s.Id).First(),
          YearOfStudyId = db.YearOfStudies.Where(e => e.Year == currentStudyYear).Select(s => s.Id).First()
        };
        db.TeacherSubjects.Add(newTeacherSubject);
        db.SaveChanges();
      }
      else
      {
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
          "Вы уже ведете этот предмет в этом классе");
      }
      return Request.CreateResponse(HttpStatusCode.Created);
    }

    [HttpGet]
    [Route("geteducation")]
    public HttpResponseMessage GetEducation(int teacherId)
    {
      var education = db.EducationTeachers.Where(e => e.TeacherId == teacherId)
        .Select(s => new
        {
          whatGraduated = s.Institution,
          date = s.Year_of_graduating,
          educationId = s.EducationId,
          specialty = s.DiplomSpecialty
        }).FirstOrDefault();
      if (education != null)
        return Request.CreateResponse(HttpStatusCode.OK, education);
      else
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Не заполнено");
    }

    [HttpGet]
    [Route("getattestation")]
    public HttpResponseMessage GetAttestation(int teacherId)
    {
        var attestation = db.Teachers.Where(e => e.Id == teacherId)
        .Select(s => new
        {
          s.Attestation.Name,
          s.Attestation.Date,
          s.Attestation.Place,
          s.Attestation.ActNumber
        }).FirstOrDefault();
      if (attestation.Name != null)
        return Request.CreateResponse(HttpStatusCode.OK, attestation);
      else
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Не заполнено");
    }

    [HttpGet]
    [Route("getownclass")]
    public HttpResponseMessage GetOwnClass(int teacherId)
    {
      var classInfo = db.TeacherClasses.Where(e => e.TeacherId == teacherId)
        .Select(s => new
        {
          ClassName = s.Class.Name,
          s.Cabinet
        }).FirstOrDefault();
      if (classInfo != null)
        return Request.CreateResponse(HttpStatusCode.OK, classInfo);
      else
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Не заполнено");
    }

    [HttpGet]
    [Route("getclasssubjects")]
    public HttpResponseMessage GetClassSubjects(int teacherId)
    {
      var classSubjects = db.TeacherSubjects.Where(e => e.TeacherId == teacherId)
        .Select(s => new
        {
          subjectName = s.Subject.Name,
          className = s.Class.Name
        }).ToList();
      if(classSubjects != null)
        return Request.CreateResponse(HttpStatusCode.OK, classSubjects);
      else
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Начните привязку к классам");
    }

    [HttpGet]
    [Route("getrewards")]
    public HttpResponseMessage GetRewards(int teacherId)
    {
      var rewards = db.TeacherRewards.Where(e => e.TeacherId == teacherId)
        .Select(s => s.Reward.Name).ToList();
      if(rewards != null)
        return Request.CreateResponse(HttpStatusCode.OK, rewards);
      else
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "У вас пока нет наград");
    }

    [HttpGet]
    [Route("getqualifications")]
    public HttpResponseMessage GetQualifications(int teacherId)
    {
      var qualifications = db.TeacherQualifications.Where(e => e.TeacherId == teacherId)
        .Select(s => new 
        {
          s.Qualification.Count_of_hours,
          s.Qualification.Course_theme,
          s.Qualification.Date_of_leaving,
          s.Qualification.Date_of_start,
          s.Qualification.Number_of_certification,
          s.Qualification.Studiyng_model,
          s.Qualification.Organisation_name          
        }).ToList();
      if (qualifications != null)
        return Request.CreateResponse(HttpStatusCode.OK, qualifications);
      else
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "У вас пока нет пройденных квалификаций");
    }

    [HttpGet]
    [Route("getdissexperience")]
    public HttpResponseMessage GetDissExperience(int teacherId)
    {
      var dissExp = db.DisseminationExperienceTeachers.Where(e => e.TeacherId == teacherId)
        .Select(s => new
        {
          s.DisseminationExperience.Level_of_event,
          s.DisseminationExperience.Date,
          s.DisseminationExperience.Name_of_event,
          s.DisseminationExperience.Theme_of_speech,
          s.DisseminationExperience.Type_of_event,
          s.DisseminationExperience.Form_of_view
        }).ToList();
      if (dissExp != null)
        return Request.CreateResponse(HttpStatusCode.OK, dissExp);
      else
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "У вас пока нет мероприятий по распространению опыта");
    }

    [HttpGet]
    [Route("getpublications")]
    public HttpResponseMessage GetPublications(int teacherId)
    {
      var publications = db.TeacherPublications.Where(e => e.TeacherId == teacherId)
        .Select(s => new
        {
          s.Publication.Level,
          s.Publication.Publishing,
          s.Publication.UrlLink,
          s.Publication.Year,
          s.Publication.Author,
          s.Publication.ArticleName
        }).ToList();
      if (publications != null)
        return Request.CreateResponse(HttpStatusCode.OK, publications);
      else
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "У вас пока нет публикаций");
    }
    //TODO: Избавиться от вспомогательных списков
    [HttpGet]
    [Route("getexperiences")]
    public HttpResponseMessage GetExperiences(int teacherId)
    {
      ExperienceModel ExperienceModel = new ExperienceModel();
      ExperienceModel.experienceList = db.TeacherExperiences
        .Where(e => e.TeacherId == teacherId)
        .Select(s => new ExperienceModel.Experience
        {
          Date_of_start = s.Experience.Date_of_start,
          Date_of_leaving = s.Experience.Date_of_leaving,
          PedExperience = s.PedExperience
        }).ToList();

      if (ExperienceModel.experienceList.Count == 0)
        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Список пуст");

      List<DateTime> pedExpList = new List<DateTime>();

      foreach (var exp in ExperienceModel.experienceList)
      {
        if (exp.Date_of_leaving != null && exp.PedExperience == true)
        {
          pedExpList.Add(exp.Date_of_start.Value);
          pedExpList.Add(exp.Date_of_leaving.Value);
        }
        else if(exp.PedExperience == true)
        {
          pedExpList.Add(exp.Date_of_start.Value);
          pedExpList.Add(DateTime.Now);
        }
      }

      AllExperience pedExperience = new AllExperience();
      for (var i = 0; i < pedExpList.Capacity / 2; i++)
      {
        pedExperience.SetDateDiff(pedExpList[i], pedExpList[i + 1]);
      }

      List<DateTime> genExpList = new List<DateTime>();
      foreach (var exp in ExperienceModel.experienceList)
      {
        if (exp.Date_of_leaving != null && exp.PedExperience == false)
        {
          genExpList.Add(exp.Date_of_start.Value);
          genExpList.Add(exp.Date_of_leaving.Value);
        }
        else if(exp.PedExperience == false)
        {
          genExpList.Add(exp.Date_of_start.Value);
          genExpList.Add(DateTime.Now);
        }
      }
      AllExperience genExperience = pedExperience;
      for (var i = 0; i < genExpList.Capacity / 2; i++)
      {
        genExperience.SetDateDiff(pedExpList[i], pedExpList[i + 1]);
      }
      ExperienceModel.pedExperienceYears = pedExperience.Years;
      ExperienceModel.pedExperienceMonths = pedExperience.Months;
      ExperienceModel.pedExperienceDays = pedExperience.Days;
      ExperienceModel.genExperienceYears = genExperience.Years;
      ExperienceModel.genExperienceMonths = genExperience.Months;
      ExperienceModel.genExperienceDays = genExperience.Days;

      return Request.CreateResponse(HttpStatusCode.OK, ExperienceModel);
    }

    private class AllExperience
    {
      public int Years;
      public int Months;
      public int Days;

      public string GetDate()
      {
        string yearWord, monthWord, dayWord;
        if (Years > 4 && Years < 21 || Years % 10 > 4)
          yearWord = "лет";
        else if (Years % 10 == 1)
          yearWord = "год";
        else
          yearWord = "года";

        if (Months > 4)
          monthWord = "месяцев";
        else if (Months > 1)
          monthWord = "месяца";
        else
          monthWord = "месяц";

        if (Days > 4 && Days < 21 || Days > 24 && Days < 31)
          dayWord = "дней";
        else if (Days % 10 == 1)
          dayWord = "день";
        else
          dayWord = "дня";

        return $"{Years} {yearWord} {Months} {monthWord} и {Days} {dayWord}";
      }

      public AllExperience() { }

      public AllExperience(DateTime Bday)
      {
        this.Count(Bday);
      }

      public AllExperience(DateTime Bday, DateTime Cday)
      {
        this.SetDateDiff(Bday, Cday);
      }

      public void Count(DateTime Bday)
      {
        this.SetDateDiff(Bday, DateTime.Today);
      }

      public void SetDateDiff(DateTime Bday, DateTime Cday)
      {
        if ((Cday.Year - Bday.Year) > 0 ||
            (((Cday.Year - Bday.Year) == 0) && ((Bday.Month < Cday.Month) ||
              ((Bday.Month == Cday.Month) && (Bday.Day <= Cday.Day)))))
        {
          int DaysInBdayMonth = DateTime.DaysInMonth(Bday.Year, Bday.Month);
          int DaysRemain = Cday.Day + (DaysInBdayMonth - Bday.Day);

          if (Cday.Month > Bday.Month)
          {
            this.Years += Cday.Year - Bday.Year;
            this.Months += Cday.Month - (Bday.Month + 1) + Math.Abs(DaysRemain / DaysInBdayMonth);
            this.Days += (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
          }
          else if (Cday.Month == Bday.Month)
          {
            if (Cday.Day >= Bday.Day)
            {
              this.Years += Cday.Year - Bday.Year;
              this.Months += 0;
              this.Days += Cday.Day - Bday.Day;
            }
            else
            {
              this.Years += (Cday.Year - 1) - Bday.Year;
              this.Months += 11;
              this.Days += DateTime.DaysInMonth(Bday.Year, Bday.Month) - (Bday.Day - Cday.Day);
            }
          }
          else
          {
            this.Years += (Cday.Year - 1) - Bday.Year;
            this.Months += Cday.Month + (11 - Bday.Month) + Math.Abs(DaysRemain / DaysInBdayMonth);
            this.Days += (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
          }
        }
        else
        {
          throw new ArgumentException("Date must be earlier than current date");
        }
      }
    }
  }
}
