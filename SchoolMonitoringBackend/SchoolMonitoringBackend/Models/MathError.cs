﻿using System.Collections.Generic;

namespace SchoolMonitoringBackend.Models
{
  public class MathError
  {
    public MathError()
    {
      MathControlErrors = new List<MathControlError>();
    }

    public int Id { get; set; }
    public string Name { get; set; }

    public ICollection<MathControlError> MathControlErrors { get; set; }
  }
}