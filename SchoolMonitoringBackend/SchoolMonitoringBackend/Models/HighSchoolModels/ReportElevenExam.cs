﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.HighSchoolModels
{
    public class ReportElevenExam
    {
        public string ClassName { get; set; }
        public int PupilsCount { get; set; }
        public string TeacherName { get; set; }
        public string SubjectName { get; set; }
        public string Year { get; set; }
        public int MemberPupilsCount { get; set; }
        public string MemberPupilsPercent { get; set; }
        public int ToMinimumCount { get; set; }
        public string ToMininmumPercent { get; set; }
        public int ToFiftyCount { get; set; }
        public string ToFiftyPercent { get; set; }
        public int ToSixtyCount { get; set; }
        public string ToSixtyPercent { get; set; }
        public int ToSeventyCount { get; set; }
        public string ToSeventyPercent { get; set; }
        public int ToEightyCount { get; set; }
        public string ToEightyPercent { get; set; }
        public int ToNintyCount { get; set; }
        public string ToNintyPercent { get; set; }
        public int ToHundredCount { get; set; }
        public string ToHundredPercent { get; set; }
        public int HundredCount { get; set; }
        public string HundredPercent { get; set; }
    }
}