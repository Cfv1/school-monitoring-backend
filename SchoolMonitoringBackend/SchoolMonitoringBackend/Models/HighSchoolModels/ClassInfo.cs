﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.HighSchoolModels
{
    public class ClassInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PupilsCount { get; set; }
        public string TeacherName { get; set; }

    }
}