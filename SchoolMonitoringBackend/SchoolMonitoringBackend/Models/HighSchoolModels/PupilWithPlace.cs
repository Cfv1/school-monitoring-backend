﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.HighSchoolModels
{
    public class PupilWithPlace
    {
        public int PupilId { get; set; }
        public string PupilName { get; set; }
        public string PupilSurname { get; set; }
        public string Place { get; set; }
    }
}