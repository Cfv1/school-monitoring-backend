﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.HighSchoolModels
{
    public class ReportEvent
    {
        public string PupilName { get; set; }
        public string PupilSurname { get; set; }
        public string PupilClass { get; set; }
        public string PupilPlace { get; set; }
        public string TeacherName { get; set; }
        public int TeacherExperience { get; set; }
    }
}