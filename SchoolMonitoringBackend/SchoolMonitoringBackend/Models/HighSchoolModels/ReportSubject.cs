﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.HighSchoolModels
{
    public class ReportSubject
    {
        public string SubjectName { get; set; }
        public string QuarterName { get; set; }
        public string TeacherName { get; set; }
        public string ClassName { get; set; }
        public int  PupilsCount { get; set; }
        public int FiveCount { get; set; }
        public int FourCount { get; set; }
        public int ThreeCount { get; set; }
        public int TwoCount { get; set; }
        public int AbsoluteProgress { get; set; }
        public int  QualitiveProgress { get; set; }
        public string Year { get; set; }
    }
}