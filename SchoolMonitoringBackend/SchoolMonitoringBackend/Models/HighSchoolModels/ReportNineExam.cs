﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.HighSchoolModels
{
    public class ReportNineExam
    {
        public string ClassName { get; set; }
        public int PupilsCount { get; set; }
        public string TeacherName { get; set; }
        public string SubjectName { get; set; }
        public string Year { get; set; }
        public int MemberPupilsCount { get; set; }
        public string MemberPupilsPercent { get; set; }
        public int FiveCount { get; set; }
        public string FivePercent { get; set; }
        public int FourCount { get; set; }
        public string FourPercent { get; set; }
        public int ThreeCount { get; set; }
        public string ThreePercent { get; set; }
        public int TwoCount { get; set; }
        public string TwoPercent { get; set; }
        public string AbsolutePercent { get; set; }
        public string QualitivePercent { get; set; }
    }
}