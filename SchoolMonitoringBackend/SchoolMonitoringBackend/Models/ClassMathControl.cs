﻿namespace SchoolMonitoringBackend.Models
{
  public class ClassMathControl
  {
    public int ClassId { get; set; }
    public Class Class { get; set; }
    public int QuarterId { get; set; }
    public Quarter Quarter { get; set; }
    public int MathControlId { get; set; }
    public MathControl MathControl { get; set; }
  }
}