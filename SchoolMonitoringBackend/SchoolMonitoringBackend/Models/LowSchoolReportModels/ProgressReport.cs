﻿using System;
using System.Collections.Generic;
using SchoolMonitoringBackend.Models.AuxilliaryModels;

namespace SchoolMonitoringBackend.Models.LowSchoolReportModels
{
  public class ProgressReport
  {
    public string ClassName { get; set; }
    public byte QuarterNumber { get;set; }
    public string YearOfStudy { get; set; }
    public string TeacherFullName { get; set; }
    public int StartQuarterPupils { get; set; }
    public int Men { get; set; }
    public int Women { get; set; }
    public int EnglishStudyPupils { get; set; }
    public int GermanStudyPupils { get; set; }
    public List<MovedPupil> Arriveds = new List<MovedPupil>();
    public List<MovedPupil> Retireds = new List<MovedPupil>();
    public int EndQuarterPupils { get; set; }
    public List <ProgressPeportPupil> Bad_pupils = new List<ProgressPeportPupil>();
    public List <ProgressPeportPupil> NotAttestated = new List<ProgressPeportPupil>();
    public List <ProgressPeportPupil> Onlyfive_pupils = new List<ProgressPeportPupil>();
    public List <ProgressPeportPupil> Without_threes = new List<ProgressPeportPupil>();
    public List <ProgressPeportPupil> Just_one_four_pupils = new List<ProgressPeportPupil>();
    public List <ProgressPeportPupil> Just_one_threes_pupils = new List<ProgressPeportPupil>();
  }
}