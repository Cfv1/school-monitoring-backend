﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.LowSchoolReportModels
{
    public class ReadTechickCheckReport
    {
        static byte listSize = 2;
        public string TeacherName { get; set; }
        public string ClassName { get; set; }
        public List<string> SurnameName = new List<string>(listSize);
        public List<int> WordCount = new List<int>(listSize);
        public List<int> Correctness = new List<int>(listSize);
        public List<bool> Mindfulness = new List<bool>(listSize);
        public List<byte> Evaluation = new List<byte>(listSize);
        public double AverageScore { get; set; }
        public double Performance { get; set; }
        public double QualityPerformance { get; set; }
        public int PupilsInClass { get; set; }
        public int PupilsFiveGrade { get; set; }
        public int PupilsFourGrade { get; set; }
        public int PupilsThreeGrade{ get; set; }
        public int PupilsTwoGrade { get; set; }
    }
}