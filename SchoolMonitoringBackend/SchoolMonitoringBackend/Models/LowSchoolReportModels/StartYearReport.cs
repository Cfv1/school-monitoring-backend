﻿using System;
using System.Collections.Generic;
using SchoolMonitoringBackend.Models.AuxilliaryModels;

namespace SchoolMonitoringBackend.Models.LowSchoolReportModels
{
  public class StartYearReport
  {
    public string ClassName { get; set; }
    public string Profile { get; set; }
    public string Cabinet { get; set; }
    public string Teacher { get; set; }
    public List<string> Pupils = new List<string>();
    public List<string> Birthdays = new List<string>();
    public List<int> MenBirthdays = new List<int>();
    public List<int> WomenBirthdays = new List<int>();
    public List<SummerArrived> Arriveds = new List<SummerArrived>();
    public List<MovedPupil> Retireds = new List<MovedPupil>();
    public List<string> PartialFamilyPupils = new List<string>();
    public List<string> HardFamilyPupils = new List<string>();
    public List<string> EnglishStudyPupils = new List<string>();
    public List<string> GermanStudyPupils = new List<string>();
    public string EnglishTeacher { get; set; }
    public string GermanTeacher { get; set; }
    public string IVTFirstGroupTeacher { get; set; }
    public string IVTSeconGroupTeacher { get; set; }
    public List<StartYearPupilData> PupilData = new List<StartYearPupilData>();
    public List<string> IVTFirstGroup = new List<string>();
    public List<string> IVTSecondGroup = new List<string>();
  }
}