﻿using System.Collections.Generic;
using SchoolMonitoringBackend.Models.AuxilliaryModels;

namespace SchoolMonitoringBackend.Models.LowSchoolReportModels
{
  public class MathReport
  {
    public string ClassName { get; set; }
    public string TeacherFullName { get; set; }
    public byte Quarter { get; set; }
    public string Year { get; set; }
    public int AllPupils { get; set; }
    public int ControlledPupils { get; set; }
    public int FivePupils { get; set; }
    public int FourPupils { get; set; }
    public int ThreePupils { get; set; }
    public int TwoPupils { get; set; }
    public int LogicTaskedPupils { get; set; }
    public int NoLogicTaskedPupils { get; set; }
    public double Performance { get; set; }
    public double QualityPerformance { get; set; }
    public double AverageScore { get; set; }
    public List<MathControlErrorAndCount> MathControlTaskErrors = new List<MathControlErrorAndCount>();
    public List<MathControlErrorAndCount> MathControlCalculateErrors = new List<MathControlErrorAndCount>();
  }
}