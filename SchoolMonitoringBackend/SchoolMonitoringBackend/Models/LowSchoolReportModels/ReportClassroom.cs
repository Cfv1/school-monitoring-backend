﻿using System;
using System.Collections.Generic;

namespace SchoolMonitoringBackend.Models.LowSchoolReportModels
{
    public class ReportClassroom
    {
        public string ClassName { get; set; }
        public int First_september_pupils { get; set; }
        public int Men { get; set; }
        public int Women { get; set; }
        public List<int> Outed = new List<int>(5);
        public List<int> Inputed = new List<int>(5);
        public List<int> Quarter_end_pupils = new List<int>(5);
        public List<int> Attestated_general = new List<int>(5);
        public List<int> Onlyfive_pupils = new List<int>(5);
        public List<int> Just_one_four_pupils = new List<int>(5);
        public List<int> Without_threes = new List<int>(5);
        public List<double> QualityPerformance = new List<double>(5);
        public List<double> Performance = new List<double>(5);
        public List<int> Bad_pupils_general = new List<int>(5);
        public List<int> Bad_pupils_1 = new List<int>(5); //не успевают по 1 предмету
        public List<int> Bad_pupils_2 = new List<int>(5);
        public List<int> Bad_pupils_more = new List<int>(5);
        public List<int> Just_one_threes_pupils = new List<int>(5);
        public List<int> Just_two_threes_pupils = new List<int>(5);
        public int Stupids { get; set; }

    }
}