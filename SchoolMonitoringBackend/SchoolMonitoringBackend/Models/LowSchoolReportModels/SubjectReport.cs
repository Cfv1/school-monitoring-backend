﻿namespace SchoolMonitoringBackend.Models.LowSchoolReportModels
{
  public class SubjectReport
  {
    public string ClassName { get; set; }
    public string TeacherFullName { get; set; }
    public string Subject { get; set; }
    public string YearName { get; set; }    
    public int AllPupils { get; set; }
    public int FivePupils { get; set; }
    public int FourPupils { get; set; }
    public int ThreePupils { get; set; }
    public int TwoPupils { get; set; }
    public double AveragePerformance { get; set; }
    public double QualityPerformance { get; set; }
    public double AverageScore { get; set; }
  }
}