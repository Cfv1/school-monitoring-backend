namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Arrived
    {
        public Arrived()
        {
            Pupils = new List<Pupil>();
        }

        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_of_arrival { get; set; }

        public string Place { get; set; }

        public virtual ICollection<Pupil> Pupils { get; set; }

        public int? QuarterId { get; set; }
        public virtual Quarter Quarter { get; set; }
    }
}
