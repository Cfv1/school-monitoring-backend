﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
    public class EventPupil
    {
        public int EventForCleverId { get; set; }
        public virtual EventForClever EventForClever { get; set; }

        public int PupilId { get; set; }
        public virtual Pupil Pupil { get; set; }

        public int? TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }

        public string Place { get; set; }
    }
}