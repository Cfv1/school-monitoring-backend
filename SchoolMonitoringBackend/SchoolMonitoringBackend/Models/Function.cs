﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
    public class Function
    {
        public Function()
        {
            Teachers = new List<Teacher>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}