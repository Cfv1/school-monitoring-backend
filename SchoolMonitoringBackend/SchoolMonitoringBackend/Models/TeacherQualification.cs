﻿namespace SchoolMonitoringBackend.Models
{
  public class TeacherQualification
  {
    public int TeacherId { get; set; }
    public int QualificationId { get; set; }
    public Teacher Teacher { get; set; }
    public Qualification Qualification { get; set; }
  }
}