﻿using System;

namespace SchoolMonitoringBackend.Models
{
    public class SecondPupil
    {
        public int Id { get; set; }
        public int PupilId { get; set; }
        public int YearOfStudyId { get; set; }

        public virtual Pupil Pupils { get; set; }

        public virtual YearOfStudy YearOfStudy { get; set; }
    }
}