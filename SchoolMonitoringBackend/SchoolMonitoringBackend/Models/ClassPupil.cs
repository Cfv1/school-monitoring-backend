﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolMonitoringBackend.Models
{
    public class ClassPupil
    {
        public int PupilId { get; set; }

        public int YearOfStudyId { get; set; }

        public virtual Pupil Pupil { get; set; }

        public virtual YearOfStudy YearOfStudy { get; set; }

        public int ClassId { get; set; }

        public virtual Class Class { get; set; }
    }
}