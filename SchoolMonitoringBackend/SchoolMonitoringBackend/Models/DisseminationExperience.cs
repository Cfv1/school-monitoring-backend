namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class DisseminationExperience
    {
        public DisseminationExperience()
        {
            DisseminationExperienceTeachers = new List<DisseminationExperienceTeacher>();
        }

        public int Id { get; set; }

        public string Type_of_event { get; set; }

        public string Name_of_event { get; set; }

        public string Level_of_event { get; set; }

        public string Form_of_view { get; set; }

        public string Theme_of_speech { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date { get; set; }

    public virtual ICollection<DisseminationExperienceTeacher> DisseminationExperienceTeachers { get; set; }
    }
}
