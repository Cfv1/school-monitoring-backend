namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("YearOfStudy")]
    public partial class YearOfStudy
    {
        public YearOfStudy()
        {
            PupilGrades = new List<PupilGrade>();
            Quarters = new List<Quarter>();
            EventForClevers = new List<EventForClever>();
            PupilSubjectGroups = new List<PupilSubjectGroup>();
            TeacherGroups = new List<TeacherGroup>();
            TeacherSubjects = new List<TeacherSubject>();
    }

        public int Id { get; set; }

        [Required]
        public string Year { get; set; }

        public virtual ICollection<PupilGrade> PupilGrades { get; set; }

        public virtual ICollection<Quarter> Quarters { get; set; }

        public virtual ICollection<Exam> Exams { get; set; }

        public virtual ICollection<EventForClever> EventForClevers { get; set; }

        public virtual ICollection<SecondPupil> SecondPupils { get; set; }

        public virtual ICollection<PupilReadTechnique> PupilReadTechniques { get; set; }

        public virtual ICollection<ClassPupil> ClassPupils { get; set; }

    public virtual ICollection<TeacherClass> TeacherClasses { get; set; }

    public virtual ICollection<PupilSubjectGroup> PupilSubjectGroups { get; set; }

    public virtual ICollection<TeacherGroup> TeacherGroups { get; set; }

    public virtual ICollection<TeacherSubject> TeacherSubjects { get; set; }

  }
}
