﻿namespace SchoolMonitoringBackend.Models
{
  public class TeacherGroup
  {
    public int TeacherId { get; set; }
    public Teacher Teacher { get; set; }
    public int GroupId { get; set; }
    public Group Group { get; set; }
    public int YearOfStudyId { get; set; }
    public YearOfStudy YearOfStudy { get; set; }
    public int ClassId { get; set; }
    public Class Class { get; set; }
  }
}