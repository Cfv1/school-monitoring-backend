﻿using System.Collections.Generic;
namespace SchoolMonitoringBackend.Models
{
  public class Group
  {
    public Group()
    {
      TeacherGroups = new List<TeacherGroup>();
    }
    public int Id { get; set; }
    public string Name { get; set; }
    public ICollection<TeacherGroup> TeacherGroups { get; set; }
  }
}