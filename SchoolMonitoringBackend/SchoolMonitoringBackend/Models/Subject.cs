namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Subject
    {
        public Subject()
        {
            PupilGrades = new List<PupilGrade>();
            EventForClevers = new List<EventForClever>();
            PupilSubjectGroups = new List<PupilSubjectGroup>();
    }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool? LowSchool { get; set; } 

        public virtual ICollection<PupilGrade> PupilGrades { get; set; }

        public virtual ICollection<TeacherSubject> TeacherSubjects { get; set; }

        public virtual ICollection<Exam> Exams { get; set; }

        public ICollection<EventForClever> EventForClevers { get; set; }

        public virtual ICollection<PupilSubjectGroup> PupilSubjectGroups { get; set; }


  }
}
