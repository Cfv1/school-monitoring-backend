﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace SchoolMonitoringBackend.Models
{
    public class ExamType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Exam> Exams { get; set; }
        public ExamType()
        {
            Exams = new HashSet<Exam>();
        }
    }
}