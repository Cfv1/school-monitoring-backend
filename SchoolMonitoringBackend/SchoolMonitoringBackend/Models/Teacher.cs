namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Teacher
    {
        public Teacher()
        {
            EducationTeachers = new List<EducationTeacher>();
            EventPupils = new List<EventPupil>();
            TeacherClasses = new List<TeacherClass>();
            TeacherGroups = new List<TeacherGroup>();
            TeacherExperiences = new List<TeacherExperience>();
            DisseminationExperienceTeachers = new List<DisseminationExperienceTeacher>();
            TeacherQualifications = new List<TeacherQualification>();
            TeacherPublications = new List<TeacherPublication>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Second_name { get; set; }

        public string Gender { get; set; }

        public DateTime? LastVisit { get; set; }

        public int? DataUserId { get; set; }

        public int? AttestationId { get; set; }

        public virtual Attestation Attestation { get; set; }

        public virtual ICollection<EducationTeacher> EducationTeachers { get; set; }

        public virtual DataUser DataUser { get; set; }

        public virtual ICollection<TeacherPublication> TeacherPublications  { get; set; }

        public virtual ICollection<TeacherReward> TeacherRewards { get; set; }

        public virtual ICollection<TeacherSubject> TeacherSubjects { get; set; }

        public virtual ICollection<EventPupil> EventPupils { get; set; }

        public virtual ICollection<TeacherClass> TeacherClasses { get; set; }

    public virtual ICollection<TeacherGroup> TeacherGroups { get; set; }

    public virtual ICollection<TeacherExperience> TeacherExperiences { get; set; }

    public virtual ICollection<DisseminationExperienceTeacher> DisseminationExperienceTeachers { get; set; }

    public virtual ICollection<TeacherQualification> TeacherQualifications { get; set; }
    }
}
