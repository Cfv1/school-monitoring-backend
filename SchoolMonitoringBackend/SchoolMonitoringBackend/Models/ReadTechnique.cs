﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
    public class ReadTechnique
    {
        public int Id { get; set; }
        public int WordCount { get; set; }
        public int Correctness { get; set; }
        public bool Mindfulness { get; set; }
        public byte Evaluation { get; set; }

        public virtual ICollection<PupilReadTechnique> PupilReadTechniques { get; set; }
    }
}