namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class EducationTeacher
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EducationId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TeacherId { get; set; }

        public int Year_of_graduating { get; set; }

        public string Institution { get; set; }

        public string DiplomSpecialty { get; set; }

        public virtual Education Education { get; set; }

        public virtual Teacher Teacher { get; set; }
    }
}
