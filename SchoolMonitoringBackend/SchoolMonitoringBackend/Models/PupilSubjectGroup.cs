﻿namespace SchoolMonitoringBackend.Models
{
  public class PupilSubjectGroup
  {
    public int PupilId { get; set; }
    public Pupil Pupil { get; set; }
    public int SubjectId { get; set; }
    public Subject Subject { get; set; }
    public int YearOfStudyId { get; set; }
    public YearOfStudy YearOfStudy { get; set; }
    public int GroupId { get; set; }
    public Group Group { get; set; }
  }
}