﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolMonitoringBackend.Models
{
    public class PupilReadTechnique
    {
        public int PupilId { get; set; }
        public int YearOfStudyId { get; set; }
        public int QuarterId { get; set; }
        public int ReadTechniqueId { get; set; }

        public virtual Pupil Pupil { get; set; }

        public virtual Quarter Quarter { get; set; }

        public virtual ReadTechnique ReadTechnique { get; set; }

        public virtual YearOfStudy YearOfStudy { get; set; }
    }
}