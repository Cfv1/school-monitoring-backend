﻿using System;
using System.Collections.Generic;

namespace SchoolMonitoringBackend.Models
{
  public class MathControl
  {
    public MathControl()
    {
      ClassMathControls = new List<ClassMathControl>();
      MathControlErrors = new List<MathControlError>();
    }

    public int Id { get; set; }
    public byte? FiveGrade { get; set; }
    public byte? FourGrade { get; set; }
    public byte? ThreeGrade { get; set; }
    public byte? TwoGrade { get; set; }
    public byte? LogicTask { get; set; }

    public virtual ICollection<ClassMathControl> ClassMathControls { get; set; }
    public ICollection<MathControlError> MathControlErrors { get; set; }
  }
}