namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Qualification
    {
        public Qualification()
        {
          TeacherQualifications = new List<TeacherQualification>();
        }

        public int Id { get; set; }

        public string Course_theme { get; set; }

        public string Organisation_name { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_of_start { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_of_leaving { get; set; }

        public int? Count_of_hours { get; set; }

        public string Studiyng_model { get; set; }

        public int Number_of_certification { get; set; }

        public virtual ICollection<TeacherQualification> TeacherQualifications { get; set; }
    }
}
