﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
    public class EventForClever
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Month { get; set; }

        public int? EventLevelId { get; set; }
        public virtual EventLevel EventLevel { get; set; }

        public int? EventTypeId { get; set; }
        public virtual EventType EventType { get; set; }

        public int? SubjectId { get; set; }
        public virtual Subject Subject { get; set; }

        public int? QuarterId { get; set; }
        public virtual Quarter Quarter { get; set; }

        public int? YearOfStudyId { get; set; }
        public virtual YearOfStudy YearOfStudy { get; set; }

        public virtual ICollection<EventPupil> EventPupils { get; set; }
    }
}