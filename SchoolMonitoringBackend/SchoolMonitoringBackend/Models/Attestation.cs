namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class Attestation
    {
        public Attestation()
        {
            Teachers = new List<Teacher>();
        }

        public int Id { get; set; }

        public string Name { get; set; }        

        [Column(TypeName = "date")]
        public DateTime? Date { get; set; }

        public string Place { get; set; }

    public string ActNumber { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
