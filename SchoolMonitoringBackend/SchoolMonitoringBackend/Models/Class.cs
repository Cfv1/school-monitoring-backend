namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Class
    {
        public Class()
        {
            ClassPupils = new List<ClassPupil>();
            TeacherSubjects = new List<TeacherSubject>();
            TeacherClasses = new List<TeacherClass>();
            TeacherGroups = new List<TeacherGroup>();
            ClassMathControls = new List<ClassMathControl>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Profile { get; set; }

        public virtual ICollection<TeacherSubject> TeacherSubjects { get; set; }

        public virtual ICollection<ClassPupil> ClassPupils { get; set; }

        public virtual ICollection<TeacherClass> TeacherClasses { get; set; }

        public virtual ICollection<TeacherGroup> TeacherGroups { get; set; }

        public virtual ICollection<ClassMathControl> ClassMathControls { get; set; }
    }
}
