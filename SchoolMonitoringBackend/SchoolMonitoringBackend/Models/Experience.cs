namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Experience
    {
        public Experience()
        {
          TeacherExperiences = new List<TeacherExperience>();
        }

        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_of_start { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_of_leaving { get; set; }

        public virtual ICollection<TeacherExperience> TeacherExperiences { get; set; }
    }
}
