﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
    public class EventLevel
    {
        public EventLevel()
        {
            EventForClevers = new List<EventForClever>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<EventForClever> EventForClevers { get; set; }
    }
}