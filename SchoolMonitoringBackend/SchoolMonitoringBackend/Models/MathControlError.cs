﻿namespace SchoolMonitoringBackend.Models
{
  public class MathControlError
  {
    public int MathControlId { get; set; }
    public MathControl MathControl { get; set; }
    public int MathErrorId { get; set; }
    public MathError MathError { get; set; }
    public int ErrorCount { get; set; }
  }
}