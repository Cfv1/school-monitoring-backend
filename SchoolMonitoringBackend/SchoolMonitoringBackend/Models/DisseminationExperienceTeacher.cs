﻿namespace SchoolMonitoringBackend.Models
{
  public class DisseminationExperienceTeacher
  {
    public int TeacherId { get; set; }
    public int DissExperienceId { get; set; }
    public Teacher Teacher { get; set; }
    public DisseminationExperience DisseminationExperience { get; set; }
  }
}