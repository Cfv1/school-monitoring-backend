namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    
    public partial class Education
    {
        public Education()
        {
            EducationTeachers = new List<EducationTeacher>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<EducationTeacher> EducationTeachers { get; set; }
    }
}
