﻿namespace SchoolMonitoringBackend.Models.AuxilliaryModels
{
  public class MathControlErrorAndCount
  {
    public string ErrorName { get; set; }
    public int Count { get; set; }
  }
}