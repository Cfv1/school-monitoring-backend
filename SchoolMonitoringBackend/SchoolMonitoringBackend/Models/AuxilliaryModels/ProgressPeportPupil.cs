﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.AuxilliaryModels
{
  public class ProgressPeportPupil
  {
    public string Surname { get; set; }
    public string Name { get; set; }
    public string SecondName { get; set; }
    public string Subject { get; set; }
  }
}