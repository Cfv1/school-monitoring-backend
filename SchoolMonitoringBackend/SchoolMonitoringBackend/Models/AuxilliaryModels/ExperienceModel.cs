﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.AuxilliaryModels
{
  public class ExperienceModel
  {
    public List<Experience> experienceList = new List<Experience>();
    public int pedExperienceYears { get; set; }
    public int pedExperienceMonths { get; set; }
    public int pedExperienceDays { get; set; }
    public int genExperienceYears { get; set; }
    public int genExperienceMonths { get; set; }
    public int genExperienceDays { get; set; }

    public class Experience
    {
      public DateTime? Date_of_start { get; set; }
      public DateTime? Date_of_leaving { get; set; }
      public bool PedExperience { get; set; }
    }
  }
}