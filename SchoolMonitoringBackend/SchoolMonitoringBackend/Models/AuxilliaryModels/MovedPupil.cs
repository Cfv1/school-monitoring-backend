﻿using System;
namespace SchoolMonitoringBackend.Models.AuxilliaryModels
{
  public class MovedPupil
  {
    public string Surname { get; set; }
    public string Name { get; set; }
    public string SecondName { get; set; }
    public string Place { get; set; }
    public DateTime DateMoved { get; set; }
    public int OrderNumber { get; set; }
  }
}