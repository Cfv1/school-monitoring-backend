﻿namespace SchoolMonitoringBackend.Models.AuxilliaryModels
{
  public class LoginData
  {
    public int TeacherId { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
    public string RoleName { get; set; }
    public string keyWord { get; set; }
  }
}