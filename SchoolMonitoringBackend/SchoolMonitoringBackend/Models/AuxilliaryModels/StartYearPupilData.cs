﻿using System;
using System.Collections.Generic;

namespace SchoolMonitoringBackend.Models.AuxilliaryModels
{
  public class StartYearPupilData
  {
    public string Surname { get; set; }
    public string Name { get; set; }
    public string SecondName { get; set; }
    public DateTime BirthDate { get; set; }
    public List<Parent> Parents = new List<Parent>();
  }
}