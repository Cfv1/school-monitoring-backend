﻿using System;
using System.Collections.Generic;

namespace SchoolMonitoringBackend.Models.TeacherPanelModels
{
  public class SearchbyFIO
  {
    public string Name { get; set; }
    public string Surname { get; set; }
    public string Second_name { get; set; }
    public string Education { get; set; }
    public DateTime? AttestationDate { get; set; }
    public string CathegoryName { get; set; }
    public List<string> Rewards = new List<string>();
  }
}