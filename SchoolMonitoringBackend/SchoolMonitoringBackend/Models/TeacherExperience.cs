﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
  public class TeacherExperience
  {
    public int TeacherId { get; set; }
    public Teacher Teacher { get; set; }
    public int ExperienceId { get; set; }
    public Experience Experience { get; set; }
    public bool PedExperience { get; set; }
  }
}