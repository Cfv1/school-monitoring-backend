﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.ViewModels
{
  public class AddRewardViewModel
  {
    public int TeacherId { get; set; }
    public int RewardId { get; set; }
  }
}