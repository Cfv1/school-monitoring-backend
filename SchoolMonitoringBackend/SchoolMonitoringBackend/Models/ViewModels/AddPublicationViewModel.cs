﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.ViewModels
{
  public class AddPublicationViewModel
  {
    public string author { get; set; }
    public string articleName { get; set; }
    public string publication { get; set; }
    public int year { get; set; }
    public string link { get; set; }
    public string level { get; set; }
    public int TeacherId { get; set; }
  }
}