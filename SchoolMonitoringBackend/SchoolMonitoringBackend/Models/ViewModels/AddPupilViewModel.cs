﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolMonitoringBackend.Models.ViewModels
{
    public class AddPupilViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Second_name { get; set; }

        public int TeacherId { get; set; }

        public string Gender { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date_of_birth { get; set; }
        [Column(TypeName = "date")]
        public DateTime ArrivedDate { get; set; }

        public string Place { get; set; }
    }
}