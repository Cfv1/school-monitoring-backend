﻿using System;

namespace SchoolMonitoringBackend.Models.ViewModels
{
  public class ChangeClassViewModel
  {
    public int TeacherId { get; set; }
    public string ClassName { get; set; }
    public string Cabinet { get; set; }
  }
}