﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.ViewModels
{
  public class EditSubjectViewModel
  {
    public int SubjectId { get; set; }
    public string ClassName { get; set; }
    public int TeacherId { get; set; }
  }
}