﻿using System;
namespace SchoolMonitoringBackend.Models.ViewModels
{
  public class AddQualificationViewModel
  {
    public int TeacherId { get; set; }
    public DateTime DateOfStart { get;  set; }
    public DateTime DateOfEnding { get;  set; }
    public int CountOfHours { get;  set; }
    public string StudyingModel { get;  set; }
    public int NumberOfCertification { get;  set; }
    public string CourseTheme { get;  set; }
    public string OrganisationName { get;  set; }
  }
}