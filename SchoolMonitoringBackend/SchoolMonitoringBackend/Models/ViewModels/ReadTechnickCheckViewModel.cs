﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models.ViewModels
{
    public class ReadTechnickCheckViewModel
    {
        public byte quarterName { get; set; }
        public int yearId { get; set; }
        public string className { get; set; }
    }
}