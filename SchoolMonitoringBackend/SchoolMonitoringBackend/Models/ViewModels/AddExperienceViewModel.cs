﻿using System;
using System.Collections.Generic;

namespace SchoolMonitoringBackend.Models.ViewModels
{
  public class AddExperienceViewModel
  {
    public int TeacherId { get; set; }
    public DateTime Date_of_start { get; set; }
    public DateTime? Date_of_leaving { get; set; }
    public bool PedExperience { get; set; }
  }
}