﻿using System;
namespace SchoolMonitoringBackend.Models.ViewModels
{
  public class EditAttestationViewModel
  {
    public int TeacherId { get; set; }
    public string Name { get; set; }
    public DateTime? Date { get; set; }
    public string Place { get; set; }
    public string ActNumber { get; set; }
  }
}