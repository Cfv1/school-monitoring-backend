﻿using System;
namespace SchoolMonitoringBackend.Models.ViewModels
{
  public class AddDissExperienceViewModel
  {
    public int TeacherId { get; set; }
    public string type { get; set; }
    public string name { get;set; }
    public string level { get;set; }
    public string formofView { get; set; }
    public string themeSpeech { get; set; }
    public DateTime date { get; set; }
  }
}
    