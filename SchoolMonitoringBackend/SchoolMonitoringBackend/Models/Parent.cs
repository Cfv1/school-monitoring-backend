﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
  public class Parent
  {
    public int Id { get; set; }
    public string Surname { get; set; }
    public string Name { get; set; }
    public string SecondName { get; set; }
    public string Phone { get; set; }

    public virtual ICollection<PupilParent> PupilParents { get; set; }
  }
}