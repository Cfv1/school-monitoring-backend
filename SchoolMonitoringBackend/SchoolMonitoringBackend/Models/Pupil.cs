namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Pupil
    {
        public Pupil()
        {
            PupilGrades = new List<PupilGrade>();
            ClassPupils = new List<ClassPupil>();
            PupilParents = new List<PupilParent>();
            PupilSubjectGroups = new List<PupilSubjectGroup>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Second_name { get; set; }

        public string Gender { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date_of_birth { get; set; }

        public int? ArrivedId { get; set; }
        public virtual Arrived Arrived { get; set; }

        public int? RetiredId { get; set; }
        public virtual Retired Retired { get; set; }

        public bool? HardFamily { get; set; }
        public bool? PartialFamily { get; set; }
        public bool? SecondYear { get; set; }

        public virtual ICollection<PupilGrade> PupilGrades { get; set; }

        public virtual ICollection<Exam> Exams { get; set; }

        public virtual ICollection<EventPupil> EventPupils { get; set; }

        public virtual ICollection<SecondPupil> SecondPupils { get; set; }

        public virtual ICollection<PupilReadTechnique> PupilReadTechniques { get; set; }

        public virtual ICollection<ClassPupil> ClassPupils { get; set; }

        public virtual ICollection<PupilParent> PupilParents { get; set; }

        public virtual ICollection<PupilSubjectGroup> PupilSubjectGroups { get; set; }
    }
}
