﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace SchoolMonitoringBackend.Models
{
    public class Exam
    {
        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }
        public int PupilId { get; set; }
        public virtual Pupil Pupil { get; set; }

        public int YearOfStudyId { get; set; }
        public virtual YearOfStudy YearOfStudy { get; set; }
        public int ExamTypeId { get; set; }
        public virtual ExamType ExamType { get; set; }

        public int? Evaluation { get; set; }
        public int? Threshold { get; set; }
        public int? ThresholdMin { get; set; }
    }
}