using System.Collections.Generic;
namespace SchoolMonitoringBackend.Models
{
  public class Publication
  {
    public Publication()
    {
      TeacherPublications = new List<TeacherPublication>();
    }

    public int Id { get; set; }
    public string Author { get; set; }
    public string ArticleName { get; set; }
    public string Publishing { get; set; }
    public int Year { get; set; }
    public string UrlLink { get; set; }
    public string Level { get; set; }
    public virtual ICollection<TeacherPublication> TeacherPublications { get; set; }
  }
}
