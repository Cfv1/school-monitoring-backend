namespace SchoolMonitoringBackend.Models
{
    using System.Data.Entity;

    public partial class SchoolContext : DbContext
    {
        public SchoolContext()
            : base("DBConnection")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<Arrived> Arriveds { get; set; }
        public virtual DbSet<Attestation> Attestations { get; set; }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<DisseminationExperience> DisseminationExperiences { get; set; }
        public virtual DbSet<Education> Educations { get; set; }
        public virtual DbSet<EducationTeacher> EducationTeachers { get; set; }
        public virtual DbSet<Experience> Experiences { get; set; }
        public virtual DbSet<Publication> Publications { get; set; }
        public virtual DbSet<PupilGrade> PupilGrades { get; set; }
        public virtual DbSet<Pupil> Pupils { get; set; }
        public virtual DbSet<Qualification> Qualifications { get; set; }
        public virtual DbSet<Quarter> Quarters { get; set; }
        public virtual DbSet<Retired> Retireds { get; set; }
        public virtual DbSet<Reward> Rewards { get; set; }
        public virtual DbSet<DataUser> DataUsers { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<TeacherPublication> TeacherPublications { get; set; }
        public virtual DbSet<TeacherReward> TeacherRewards { get; set; }
        public virtual DbSet<TeacherSubject> TeacherSubjects { get; set; }
        public virtual DbSet<YearOfStudy> YearOfStudies { get; set; }
        public virtual DbSet<ExamType> ExamTypes { get; set; }
        public virtual DbSet<Exam> Exams { get; set; }
        public virtual DbSet<EventLevel> EventLevels { get; set; }
        public virtual DbSet<EventType> EventTypes { get; set; }
        public virtual DbSet<EventForClever> EventForClevers { get; set; }
        public virtual DbSet<EventPupil> EventPupils { get; set; }
        public virtual DbSet<SecondPupil> SecondPupils { get; set; }
        public virtual DbSet<ReadTechnique> ReadTechniques { get; set; }
        public virtual DbSet<PupilReadTechnique> PupilReadTechniques { get; set; }
        public virtual DbSet<ClassPupil> ClassPupils { get; set; }
        public virtual DbSet<TeacherClass> TeacherClasses { get; set; }
        public virtual DbSet<Parent> Parents { get; set; }
        public virtual DbSet<PupilParent> PupilParents { get; set; }
        public virtual DbSet<PupilSubjectGroup> PupilSubjectGroups { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<TeacherGroup> TeacherGroups {get;set;}
        public virtual DbSet<ClassMathControl> ClassMathControls { get; set; }
        public virtual DbSet<MathControl> MathControls { get; set; }
    public virtual DbSet<MathControlError> MathControlErrors { get; set; }
    public virtual DbSet<MathError> MathErrors { get; set; }
    public virtual DbSet<TeacherExperience> TeacherExperiences { get; set; }
    public virtual DbSet<TeacherQualification> TeacherQualifications { get; set; }
    public virtual DbSet<DisseminationExperienceTeacher> DisseminationExperienceTeachers { get; set; }

        // Проставление всех связей многие-ко-многим
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // EducationTeacher
            modelBuilder.Entity<Education>()
                .HasMany(e => e.EducationTeachers)
                .WithRequired(e => e.Education)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Teacher>()
                .HasMany(e => e.EducationTeachers)
                .WithRequired(e => e.Teacher)
                .WillCascadeOnDelete(false);


            // PupilGrades
            modelBuilder.Entity<PupilGrade>()
                .HasKey(e => new { e.PupilId, e.SubjectId, e.QuarterId, e.YearOfStudyId });

            modelBuilder.Entity<Pupil>()
                .HasMany(e => e.PupilGrades)
                .WithRequired(e => e.Pupil)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Subject>()
                .HasMany(e => e.PupilGrades)
                .WithRequired(e => e.Subject)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Quarter>()
                .HasMany(e => e.PupilGrades)
                .WithRequired(e => e.Quarter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YearOfStudy>()
                .HasMany(e => e.PupilGrades)
                .WithRequired(e => e.YearOfStudy)
                .WillCascadeOnDelete(false);


            // Exam
            modelBuilder.Entity<Exam>()
            .HasKey(e => new { e.PupilId, e.SubjectId, e.ExamTypeId, e.YearOfStudyId });

            modelBuilder.Entity<Subject>()
               .HasMany(op => op.Exams)
               .WithRequired(o => o.Subject)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pupil>()
                .HasMany(op => op.Exams)
                .WithRequired(p => p.Pupil)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ExamType>()
               .HasMany(op => op.Exams)
               .WithRequired(o => o.ExamType)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<YearOfStudy>()
                .HasMany(op => op.Exams)
                .WithRequired(p => p.YearOfStudy)
                .WillCascadeOnDelete(false);

            
            // TeacherPublication
            modelBuilder.Entity<TeacherPublication>()
                .HasKey(op => new { op.PublicationId, op.TeacherId });

            modelBuilder.Entity<Teacher>()
              .HasMany(op => op.TeacherPublications)
              .WithRequired(o => o.Teacher)
              .WillCascadeOnDelete(false);

            modelBuilder.Entity<Publication>()
              .HasMany(op => op.TeacherPublications)
              .WithRequired(p => p.Publication)
              .WillCascadeOnDelete(false);


            // TeacherReward
            modelBuilder.Entity<TeacherReward>()
                .HasKey(op => new { op.RewardId, op.TeacherId });

            modelBuilder.Entity<Teacher>()
               .HasMany(op => op.TeacherRewards)
               .WithRequired(p => p.Teacher)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Reward>()
              .HasMany(op => op.TeacherRewards)
              .WithRequired(p => p.Reward)
              .WillCascadeOnDelete(false);

            // TeacherSubject
            modelBuilder.Entity<TeacherSubject>()
                .HasKey(op => new { op.SubjectId, op.TeacherId });
            
            modelBuilder.Entity<Teacher>()
               .HasMany(op => op.TeacherSubjects)
               .WithRequired(p => p.Teacher)
               .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<Subject>()
               .HasMany(op => op.TeacherSubjects)
               .WithRequired(p => p.Subject)
               .WillCascadeOnDelete(false);
            

            // EventPupil (по одаренным детям)
            modelBuilder.Entity<EventPupil>()
             .HasKey(op => new { op.PupilId, op.EventForCleverId });

            modelBuilder.Entity<Pupil>()
                .HasMany(op => op.EventPupils)
                .WithRequired(o => o.Pupil)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EventForClever>()
                .HasMany(op => op.EventPupils)
                .WithRequired(p => p.EventForClever)
                .WillCascadeOnDelete(false);

            //PupilReadTechinques проверка техники чтения
            modelBuilder.Entity<PupilReadTechnique>()
                .HasKey(op => new { op.PupilId, op.QuarterId, op.YearOfStudyId, op.ReadTechniqueId });

            modelBuilder.Entity<Pupil>()
                .HasMany(op => op.PupilReadTechniques)
                .WithRequired(op => op.Pupil)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Quarter>()
                .HasMany(op => op.PupilReadTechniques)
                .WithRequired(op => op.Quarter)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YearOfStudy>()
                .HasMany(op => op.PupilReadTechniques)
                .WithRequired(op => op.YearOfStudy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ReadTechnique>()
                .HasMany(op => op.PupilReadTechniques)
                .WithRequired(op => op.ReadTechnique)
                .WillCascadeOnDelete(false);

            //ClassPupils

            modelBuilder.Entity<ClassPupil>().HasKey(o => new { o.PupilId, o.YearOfStudyId });

            modelBuilder.Entity<Pupil>()
                .HasMany(op => op.ClassPupils)
                .WithRequired(op => op.Pupil)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<YearOfStudy>()
                .HasMany(op => op.ClassPupils)
                .WithRequired(op => op.YearOfStudy)
                .WillCascadeOnDelete(false);

      //PupilParent
      modelBuilder.Entity<PupilParent>().HasKey(o => new { o.PupilId, o.ParentId });

      modelBuilder.Entity<Pupil>()
                .HasMany(op => op.PupilParents)
                .WithRequired(op => op.Pupil)
                .WillCascadeOnDelete(false);

      modelBuilder.Entity<Parent>()
                .HasMany(op => op.PupilParents)
                .WithRequired(op => op.Parent)
                .WillCascadeOnDelete(false);

      //TeacherClasses
      modelBuilder.Entity<TeacherClass>().HasKey(o => new { o.ClassId, o.YearOfStudyId });

      modelBuilder.Entity<Class>()
                .HasMany(op => op.TeacherClasses)
                .WithRequired(op => op.Class)
                .WillCascadeOnDelete(false);

      modelBuilder.Entity<YearOfStudy>()
                .HasMany(op => op.TeacherClasses)
                .WithRequired(op => op.YearOfStudy)
                .WillCascadeOnDelete(false);
      
      //pupilsubjectGroups
      modelBuilder.Entity<PupilSubjectGroup>().HasKey(k => new
      {
        k.PupilId,
        k.SubjectId,
        k.YearOfStudyId
      });

      modelBuilder.Entity<Pupil>()
                .HasMany(op => op.PupilSubjectGroups)
                .WithRequired(op => op.Pupil)
                .WillCascadeOnDelete(false);

      modelBuilder.Entity<Subject>()
                .HasMany(op => op.PupilSubjectGroups)
                .WithRequired(op => op.Subject)
                .WillCascadeOnDelete(false);

      modelBuilder.Entity<YearOfStudy>()
                .HasMany(op => op.PupilSubjectGroups)
                .WithRequired(op => op.YearOfStudy)
                .WillCascadeOnDelete(false);

      //TeacherGroups
      modelBuilder.Entity<TeacherGroup>().HasKey(k => new
      {
        k.TeacherId,
        k.GroupId,
        k.YearOfStudyId
      });

      modelBuilder.Entity<Teacher>()
        .HasMany(o => o.TeacherGroups)
        .WithRequired(o => o.Teacher)
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<Group>()
        .HasMany(o => o.TeacherGroups)
        .WithRequired(o => o.Group)
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<YearOfStudy>()
        .HasMany(o => o.TeacherGroups)
        .WithRequired(o => o.YearOfStudy)
        .WillCascadeOnDelete(false);

      //ClassMathControls
      modelBuilder.Entity<ClassMathControl>().HasKey(o => new { o.ClassId, o.QuarterId });

      modelBuilder.Entity<Class>()
        .HasMany(o => o.ClassMathControls)
        .WithRequired(o => o.Class)
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<Quarter>()
        .HasMany(o => o.ClassMathControls)
        .WithRequired(o => o.Quarter)
        .WillCascadeOnDelete(false);

      //MathControlErrors
      modelBuilder.Entity<MathControlError>().HasKey(o => new { o.MathControlId, o.MathErrorId });

      modelBuilder.Entity<MathControl>()
        .HasMany(o => o.MathControlErrors)
        .WithRequired(o => o.MathControl)
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<MathError>()
        .HasMany(o => o.MathControlErrors)
        .WithRequired(o => o.MathError)
        .WillCascadeOnDelete(false);

      //TeacherExperiences
      modelBuilder.Entity<TeacherExperience>().HasKey(o => new { o.TeacherId, o.ExperienceId });

      modelBuilder.Entity<Teacher>()
        .HasMany(o => o.TeacherExperiences)
        .WithRequired(o => o.Teacher)
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<Experience>()
        .HasMany(o => o.TeacherExperiences)
        .WithRequired(o => o.Experience)
        .WillCascadeOnDelete(false);

      //DisseminationExperienceTeachers
      modelBuilder.Entity<DisseminationExperienceTeacher>().HasKey(
        k => new { k.TeacherId, k.DissExperienceId });

      modelBuilder.Entity<Teacher>()
        .HasMany(o => o.DisseminationExperienceTeachers)
        .WithRequired(o => o.Teacher)
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<DisseminationExperience>()
        .HasMany(o => o.DisseminationExperienceTeachers)
        .WithRequired(o => o.DisseminationExperience)
        .HasForeignKey(k=> k.DissExperienceId)
        .WillCascadeOnDelete(false);

      //TeacherQualifications
      modelBuilder.Entity<TeacherQualification>().HasKey(
        k => new { k.TeacherId, k.QualificationId });

      modelBuilder.Entity<Teacher>()
        .HasMany(o => o.TeacherQualifications)
        .WithRequired(o => o.Teacher)
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<Qualification>()
        .HasMany(o => o.TeacherQualifications)
        .WithRequired(o => o.Qualification)
        .WillCascadeOnDelete(false);

      //TeacherPublications
      modelBuilder.Entity<TeacherPublication>().HasKey(
        k => new { k.TeacherId, k.PublicationId });

      modelBuilder.Entity<Teacher>()
        .HasMany(t => t.TeacherPublications)
        .WithRequired(t => t.Teacher)
        .WillCascadeOnDelete(false);

      modelBuilder.Entity<Publication>()
        .HasMany(p => p.TeacherPublications)
        .WithRequired(p => p.Publication)
        .WillCascadeOnDelete(false);
    }
  }
}
