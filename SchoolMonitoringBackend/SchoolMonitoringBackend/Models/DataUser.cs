﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
  public class DataUser
  {
    public DataUser()
    {
      Teachers = new List<Teacher>();
    }

    public int Id { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
    public string RoleName { get; set; }

    public virtual ICollection<Teacher> Teachers { get; set; }
  }
}