namespace SchoolMonitoringBackend.Models
{
    using System.Collections.Generic;

    public partial class Reward
    {
        public Reward()
        {
          TeacherRewards = new List<TeacherReward>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TeacherReward> TeacherRewards { get; set; }
    }
}
