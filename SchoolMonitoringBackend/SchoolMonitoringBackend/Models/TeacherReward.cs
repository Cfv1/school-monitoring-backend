﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
    public class TeacherReward
    {
        public int TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }

        public int RewardId { get; set; }
        public virtual Reward Reward { get; set; }
    }
}