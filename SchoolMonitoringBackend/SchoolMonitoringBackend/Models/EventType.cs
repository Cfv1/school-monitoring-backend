﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
    public class EventType
    {
        public EventType()
        {
            EventForClevers = new List<EventForClever>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<EventForClever> EventForClevers { get; set; }
    }
}