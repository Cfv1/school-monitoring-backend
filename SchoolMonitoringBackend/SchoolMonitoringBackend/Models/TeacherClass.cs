﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
  public class TeacherClass
  {
    public int ClassId { get; set; }
    public virtual Class Class { get; set; }

    public int YearOfStudyId { get; set; }
    public virtual YearOfStudy YearOfStudy { get; set; }

    public int TeacherId { get; set; }
    public virtual Teacher Teacher { get; set; }

    public string Cabinet { get; set; }
  }
}