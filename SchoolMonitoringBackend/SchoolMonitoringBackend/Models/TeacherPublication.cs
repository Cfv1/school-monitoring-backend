﻿namespace SchoolMonitoringBackend.Models
{
    public class TeacherPublication
    {
        public int TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }

        public int PublicationId { get; set; }
        public virtual Publication Publication { get; set; }
    }
}