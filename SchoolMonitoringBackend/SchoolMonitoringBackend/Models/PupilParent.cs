﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolMonitoringBackend.Models
{
  public class PupilParent
  {
    public int PupilId { get; set; }
    public virtual Pupil Pupil { get; set; }

    public int ParentId { get; set; }
    public virtual Parent Parent { get; set; }
  }
}