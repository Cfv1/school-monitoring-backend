namespace SchoolMonitoringBackend.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Quarter
    {
        public Quarter()
        {
            PupilGrades = new List<PupilGrade>();
            Arriveds = new List<Arrived>();
            Retireds = new List<Retired>();
            EventForClevers = new List<EventForClever>();
      ClassMathControls = new List<ClassMathControl>();
    }

        public int Id { get; set; }

        public byte? Name { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_of_start { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_of_ending { get; set; }

        public int YearOfStudyId { get; set; }
        public virtual YearOfStudy YearOfStudy { get; set; }

        public virtual ICollection<PupilGrade> PupilGrades { get; set; }

        public virtual ICollection<Arrived> Arriveds { get; set; }

        public virtual ICollection<Retired> Retireds { get; set; }

        public virtual ICollection<PupilReadTechnique> PupilReadTechniques { get; set; }

        public ICollection<EventForClever> EventForClevers { get; set; }

    public virtual ICollection<ClassMathControl> ClassMathControls { get; set; }

  }
}
