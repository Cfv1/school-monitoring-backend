﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchoolMonitoringBackend.Models.LowSchoolReportModels;
using SchoolMonitoringBackend.Models;

namespace SchoolMonitoringBackend.Services
{
  public static class SubjectReportService
  {
    //добавить инфу о преподе
    public static List<SubjectReport> CreateSubjectReport(byte quarterName, int yearId, string className)
    {
      SchoolContext db = new SchoolContext();
      var countSubjects = 0;
      var subjectNames = new List<string>(8);
      int withoutTwoPupils = 0;
      int goodPupils = 0;
      if (className.Contains("1"))
      {
        countSubjects = 8;
        subjectNames = db.Subjects.Where(s => !s.Name.Contains("Английский") &&
        !s.Name.Contains("Немецкий") && !s.Name.Contains("религиозных"))
        .Select(s => s.Name).ToList();
      }
      else if (className.Contains("4"))
      {
        countSubjects = 11;
        subjectNames = db.Subjects.Select(s => s.Name).ToList();
      }
      else
      {
        countSubjects = 10;
        subjectNames = db.Subjects.Where(s => !s.Name.Contains("религиозных"))
        .Select(s => s.Name).ToList();
      }

      List<SubjectReport> subjectData = new List<SubjectReport>(8);
      for(var i=0; i<countSubjects;i++)
      {
        SubjectReport tempReport = new SubjectReport();
        tempReport.ClassName = className;
        tempReport.Subject = subjectNames[i];
        tempReport.AllPupils = GetAllPupils(ref db, quarterName, yearId, className, subjectNames[i]);
        tempReport.FivePupils = GetGradesOfClass(ref db, quarterName, yearId, className, subjectNames[i], 5);
        tempReport.FourPupils = GetGradesOfClass(ref db, quarterName, yearId, className, subjectNames[i], 4);
        tempReport.ThreePupils = GetGradesOfClass(ref db, quarterName, yearId, className, subjectNames[i], 3);
        tempReport.TwoPupils = GetGradesOfClass(ref db, quarterName, yearId, className, subjectNames[i], 2);
        withoutTwoPupils = tempReport.FivePupils + tempReport.FourPupils + tempReport.ThreePupils;
        tempReport.AveragePerformance = GetPerformance(ref db, tempReport.AllPupils, withoutTwoPupils);
        goodPupils = tempReport.FivePupils + tempReport.FourPupils;
        tempReport.QualityPerformance = GetQualityPerformance(ref db, tempReport.AllPupils, goodPupils);
        tempReport.AverageScore = GetAverageScore(ref db, tempReport.FivePupils, tempReport.FourPupils, tempReport.ThreePupils, tempReport.TwoPupils);
        subjectData.Add(tempReport);
      }
      return subjectData;
    }

    //здесь по оценкам, т.е. не аттестованные не учитываются
    private static int GetAllPupils(ref SchoolContext db, byte quarterName, int yearId, string className, string subjectName)
    {
      return db.PupilGrades
              .Where(e => e.Pupil.ClassPupils.Any(c => c.Class.Name == className))
              .Where(e => e.Subject.Name == subjectName)
              .Where(e => e.Quarter.Name == quarterName)
              .Where(e => e.YearOfStudyId == yearId)
              .Count();
    }

    private static int GetGradesOfClass(ref SchoolContext db, byte quarterName, int yearId, string className, string subjectName, int grade)
    {
      return db.PupilGrades
              .Where(e => e.Pupil.ClassPupils.Any(c => c.Class.Name == className))
              .Where(e=> e.Subject.Name == subjectName)
              .Where(e => e.Quarter.Name == quarterName)
              .Where(e => e.Evaluation == grade)
              .Where(e => e.YearOfStudyId == yearId)
              .Count();
    }

    private static double GetPerformance(ref SchoolContext db, int allpupils, int withoutTwoPupils)
    {
      return Math.Round(((double)withoutTwoPupils * 100/ allpupils), 2);
    }

    private static double GetQualityPerformance(ref SchoolContext db, int allpupils, int goodPupils)
    {
      return Math.Round(((double)goodPupils * 100 / allpupils), 2);
    }

    private static double GetAverageScore(ref SchoolContext db, int fivePupils, int fourPupils, int threePupils, int twoPupils)
    {
      return Math.Round(((double)(5 * fivePupils + 4 * fourPupils + 3 * threePupils + 2 * twoPupils)/
        (fivePupils + fourPupils + threePupils + twoPupils)), 2);
    }
  }
}
