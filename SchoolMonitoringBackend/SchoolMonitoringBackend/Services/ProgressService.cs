﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SchoolMonitoringBackend.Models.LowSchoolReportModels;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.AuxilliaryModels;

namespace SchoolMonitoringBackend.Services
{
  public static class ProgressService
  {
    public static ProgressReport CreateProgressReport(byte quarterName, int yearId, string className)
    {
      SchoolContext db = new SchoolContext();
      ProgressReport report = new ProgressReport();
      var classId = db.Classes.Where(e => e.Name == className).Select(s => s.Id).First();
      var quarterDates = db.Quarters.Where(e => e.Name == quarterName)
          .Where(e=> e.YearOfStudy.Id == yearId)
          .Select(s => new {s.Id, s.Date_of_start, s.Date_of_ending }).First();
      int quarterId = quarterDates.Id;
      DateTime quarterStart = (DateTime)quarterDates.Date_of_start;
      DateTime quarterEnd = (DateTime)quarterDates.Date_of_ending;
      report.ClassName = className;
      report.QuarterNumber = quarterName;
      report.TeacherFullName = GetTeacher(ref db, classId, yearId);
      report.YearOfStudy = db.YearOfStudies.Where(e=> e.Id == yearId).Select(s => s.Year).First();
      report.StartQuarterPupils = GetStartQuarterPupils(ref db, quarterStart, classId, yearId);
      report.Men = GetMen(ref db, quarterStart, classId, yearId);
      report.Women = GetWomen(ref db, quarterStart, classId, yearId);
      report.EnglishStudyPupils = GetStudyLanguagePupils(ref db, classId, yearId, quarterId);
      report.GermanStudyPupils = GetStudyLanguagePupils(ref db, classId, yearId, quarterId, 1);
      report.Arriveds = GetArrived(ref db, quarterStart, quarterEnd, classId, yearId);
      report.Retireds = GetRetired(ref db, quarterStart, quarterEnd, classId, yearId);
      report.EndQuarterPupils = GetEndQuarterPupils(ref db, quarterEnd, classId, yearId);
      report.Bad_pupils = GetBadPupilsGeneral(ref db, classId, yearId, quarterId);
      report.Onlyfive_pupils = GetOnlyFivePupils(ref db, classId, yearId, quarterId);
      report.Without_threes = GetWithoutThreesPupils(ref db, classId, yearId, quarterId);
      report.Just_one_four_pupils = GetJustOneFourPupils(ref db, classId, yearId, quarterId);
      report.Just_one_threes_pupils = GetJustOneThreePupils(ref db, classId, yearId, quarterId);
      report.NotAttestated = GetNotAttestated(ref db, classId, yearId, quarterId);
      return report;
    }

    private static string GetTeacher(ref SchoolContext db, int classId, int yearId)
    {
        var anon_FIO = db.TeacherClasses.Where(e => e.ClassId == classId &&
          e.YearOfStudyId == yearId)
        .Select(s => new
        {
            Name = s.Teacher.Name,
            Surname = s.Teacher.Surname,
            SecondName = s.Teacher.Second_name
        }).First();
        return anon_FIO.Surname + " " + anon_FIO.Name + " " + anon_FIO.SecondName;
    }

    private static int GetStartQuarterPupils(ref SchoolContext db, DateTime quarterStart, int classId, int yearId)
    {
      return db.Pupils.Where(e => e.ClassPupils.Any(c=> c.ClassId == classId
        && c.YearOfStudyId == yearId))
        .Where(e => e.Arrived.Date_of_arrival <= quarterStart)
        .Where(e=> e.Retired.Date_of_retirement > quarterStart
        || e.Retired.Date_of_retirement == null)
        .Count();
    }

    static private int GetMen(ref SchoolContext db, DateTime quarterStart, int classId,  int yearId)
    {
      return db.Pupils.Where(e => e.ClassPupils.Any(c => c.ClassId == classId
          && c.YearOfStudyId == yearId))
          .Where(e => e.Arrived.Date_of_arrival <= quarterStart)
          .Where(e => e.Retired.Date_of_retirement > quarterStart
            || e.Retired.Date_of_retirement == null)
          .Where(e=> e.Gender == "Мужской")
          .Count();
    }

    static private int GetWomen(ref SchoolContext db, DateTime quarterStart, int classId, int yearId)
    {
      return db.Pupils.Where(e => e.ClassPupils.Any(c => c.ClassId == classId
          && c.YearOfStudyId == yearId))
          .Where(e => e.Arrived.Date_of_arrival <= quarterStart)
          .Where(e => e.Retired.Date_of_retirement > quarterStart
            || e.Retired.Date_of_retirement == null)
          .Where(e => e.Gender == "Женский")
          .Count();
    }

    //Считает по оценкам, вроде как надо по началу четверти
    static private int GetStudyLanguagePupils(ref SchoolContext db, int classId, int yearId, int quarterId, byte selector = 0)
    {
      string language = "";
      if (selector == 0)
        language = "Английский язык";
      else
        language = "Немецкий язык";
      return db.PupilGrades.Where(e => e.YearOfStudyId == yearId)
        .Where(e => e.QuarterId == quarterId)
        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
        .Where(e => e.Subject.Name.Contains(language))
        .Count();
    }

    static private List<MovedPupil> GetArrived(ref SchoolContext db, DateTime quarterStart, DateTime quarterEnd, int classId, int yearId)
    {
      List<MovedPupil> list = new List<MovedPupil>();
      var query = db.Pupils.Include(t => t.Arrived)
            .Include(t=> t.ClassPupils)
            .Where(z => z.ClassPupils.Any(c => c.ClassId == classId))
            .Where(e => e.Arrived.Date_of_arrival > quarterStart &&
              e.Arrived.Date_of_arrival < quarterEnd)
              .Select(s => new {s.Surname, s.Name, s.Second_name, s.Arrived.Date_of_arrival });
      foreach(var q in query)
      {
        list.Add(new MovedPupil() {
          Surname = q.Surname, Name = q.Name,
          SecondName = q.Second_name, DateMoved = (DateTime)q.Date_of_arrival
        });
      }
      return list;
    }

    static private List<MovedPupil> GetRetired(ref SchoolContext db, DateTime quarterStart, DateTime quarterEnd, int classId, int yearId)
    {
      List<MovedPupil> list = new List<MovedPupil>();
      var query = db.Pupils.Include(t => t.Retired)
            .Include(t=> t.ClassPupils)
            .Where(z => z.ClassPupils.Any(c => c.ClassId == classId))
            .Where(e => e.Retired.Date_of_retirement > quarterStart &&
              e.Retired.Date_of_retirement < quarterEnd)
              .Select(s => new { s.Surname, s.Name, s.Second_name, s.Retired.Date_of_retirement }); ;
      foreach (var q in query)
      {
        list.Add(new MovedPupil()
        {
          Surname = q.Surname,
          Name = q.Name,
          SecondName = q.Second_name,
          DateMoved = (DateTime)q.Date_of_retirement
        });
      }
      return list;
    }

    static private int GetEndQuarterPupils(ref SchoolContext db, DateTime quarterEnd, int classId, int yearId)
    {
      return db.Pupils.Where(e => e.ClassPupils.Any(c => c.ClassId == classId
        && c.YearOfStudyId == yearId))
        .Where(e => e.Arrived.Date_of_arrival <= quarterEnd)
        .Where(e => e.Retired.Date_of_retirement >= quarterEnd
        || e.Retired.Date_of_retirement == null)
        .Count();
    }

    static private List<ProgressPeportPupil> GetBadPupilsGeneral(ref SchoolContext db, int classId, int yearId, int quarterId)
    {
      List<ProgressPeportPupil> list = new List<ProgressPeportPupil>();
      var query = db.PupilGrades.Include(t=>t.Pupil).Include(t=>t.Pupil.ClassPupils)
              .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
              .Where(e => e.QuarterId == quarterId)
              .Where(e => e.YearOfStudyId == yearId)
              .Where(e => e.Evaluation == 2)
              .Select(s => new ProgressPeportPupil
              {
                Surname = s.Pupil.Surname,
                Name = s.Pupil.Name,
                SecondName = s.Pupil.Second_name,
                Subject = s.Subject.Name
              });

      foreach(var q in query)
      {
        list.Add(q);
      }
      return list;
    }

    //работает, но хз как вытащить предмет
    static private List<ProgressPeportPupil> GetNotAttestated(ref SchoolContext db, int classId, int yearId, int quarterId)
    {
      var list = db.Pupils
              .Where(e => e.ClassPupils.Any(c => c.ClassId == classId))
              .Where(e => !e.PupilGrades.Any(c=> c.QuarterId == quarterId &&
                c.YearOfStudyId == yearId))
              .Select(s => new ProgressPeportPupil
              {
                Surname = s.Surname,
                Name = s.Name,
                SecondName = s.Second_name,
                //Subject = s.PupilGrades.First(x=> x.Subject.Name)
              }).ToList();
      return list;
    }

    static private List<ProgressPeportPupil> GetOnlyFivePupils(ref SchoolContext db, int classId, int yearId, int quarterId)
    {
      var list = db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.QuarterId == quarterId)
                        .Where(e => e.Evaluation == 5)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Select(e => new ProgressPeportPupil
                        {
                          Surname = e.Pupil.Surname,
                          Name = e.Pupil.Name,
                          SecondName = e.Pupil.Second_name
                        })
                        .Except(
                        db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.QuarterId == quarterId)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 4)
                        .Select(e => new ProgressPeportPupil
                        {
                          Surname = e.Pupil.Surname,
                          Name = e.Pupil.Name,
                          SecondName = e.Pupil.Second_name
                        })
                        .Except(
                        db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.QuarterId == quarterId)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 3)
                        .Select(e => new ProgressPeportPupil
                        {
                          Surname = e.Pupil.Surname,
                          Name = e.Pupil.Name,
                          SecondName = e.Pupil.Second_name
                        })
                        .Except(
                        db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.QuarterId == quarterId)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 2)
                        .Select(e => new ProgressPeportPupil
                        {
                          Surname = e.Pupil.Surname,
                          Name = e.Pupil.Name,
                          SecondName = e.Pupil.Second_name
                        })))).ToList();
      return list;
    }

    static private List<ProgressPeportPupil> GetWithoutThreesPupils(ref SchoolContext db, int classId, int yearId, int quarterId)
    {
      var list = db.PupilGrades.Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.QuarterId == quarterId)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 5)
                        .Select(e => new ProgressPeportPupil
                        {
                          Surname = e.Pupil.Surname,
                          Name = e.Pupil.Name,
                          SecondName = e.Pupil.Second_name
                        })
                        .Intersect(
                        db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.QuarterId == quarterId)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 4)
                        .Select(e => new ProgressPeportPupil
                        {
                          Surname = e.Pupil.Surname,
                          Name = e.Pupil.Name,
                          SecondName = e.Pupil.Second_name
                        })).ToList();
      return list;
    }

    static private List<ProgressPeportPupil> GetJustOneFourPupils(ref SchoolContext db, int classId, int yearId, int quarterId)
    {
      var pupilIDS = db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.QuarterId == quarterId)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation == 4)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() == 1)
                    .Select(s => s.Key)
                    .Intersect(
                    db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.QuarterId == quarterId)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation == 5)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() >= 1) // заменить на >1
                    .Select(s => s.Key)).ToList();
      var list = db.Pupils.Where(e => pupilIDS.Contains(e.Id))
        .Select(s=> new ProgressPeportPupil {
          Surname = s.Surname, Name = s.Name, SecondName = s.Second_name
        }).ToList();
      return list;
    }

    static private List<ProgressPeportPupil> GetJustOneThreePupils(ref SchoolContext db, int classId, int yearId, int quarterId)
    {
      var pupilIDS = db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.QuarterId == quarterId)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation == 3)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() == 1)
                    .Select(s => s.Key)
                    .Intersect(
                    db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.QuarterId == quarterId)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation != 2)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() > 1)
                    .Select(s => s.Key)).ToList();
      var list = db.Pupils.Where(e => pupilIDS.Contains(e.Id))
        .Select(s => new ProgressPeportPupil
        {
          Surname = s.Surname,
          Name = s.Name,
          SecondName = s.Second_name
        }).ToList();
      return list;
    }

  }
}
