﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SchoolMonitoringBackend.Models.LowSchoolReportModels;
using SchoolMonitoringBackend.Models.AuxilliaryModels;
using SchoolMonitoringBackend.Models;

namespace SchoolMonitoringBackend.Services
{
  public static class MathReportService
  {
    public static MathReport CreateReport(byte quarterName, int yearId, string className)
    {
      SchoolContext db = new SchoolContext();
      var report = new MathReport();
      var quarterId = db.Quarters.Where(e => e.Name == quarterName)
        .Where(e => e.YearOfStudyId == yearId).Select(s => s.Id).First();
      int classId = db.Classes.Where(e => e.Name == className).Select(s => s.Id)
        .First();
      int mathControlId = db.ClassMathControls.Where(e => e.QuarterId == quarterId &&
        e.ClassId == classId).Select(s => s.MathControl.Id).First();
      report.Quarter = quarterName;
      report.ClassName = className;
      report.Year = GetYear(ref db, yearId);
      report.TeacherFullName = GetTeacherName(ref db, yearId, classId);
      report.AllPupils = GetAllPupils(ref db, yearId, classId);
      report.FivePupils = GetFivePupils(ref db, quarterId, classId);
      report.FourPupils = GetFourPupils(ref db, quarterId, classId);
      report.ThreePupils = GetThreePupils(ref db, quarterId, classId);
      report.TwoPupils = GetTwoPupils(ref db, quarterId, classId);
      report.ControlledPupils = GetControlledPupils(report.FivePupils, 
        report.FourPupils, report.ThreePupils, report.TwoPupils);
      report.LogicTaskedPupils = GetLogicTaskedPupils(ref db, quarterId, classId);
      report.NoLogicTaskedPupils = GetNoLogicTaskedPupils(report.ControlledPupils, report.LogicTaskedPupils);
      report.Performance = GetPerformance(report.FivePupils, report.FourPupils,
        report.ThreePupils, report.ControlledPupils);
      report.QualityPerformance = GetQualityPerformance(report.FivePupils, report.FourPupils,
        report.ControlledPupils);
      report.AverageScore = GetAverageScore(report.FivePupils,
        report.FourPupils, report.ThreePupils, report.TwoPupils);
      report.MathControlTaskErrors = GetTaskErrorsCount(ref db, mathControlId);
      report.MathControlCalculateErrors = GetCalculateErrorsCount(ref db, mathControlId);
      return report;
    }

    private static string GetYear(ref SchoolContext db, int yearId)
    {
      return db.YearOfStudies.Where(e => e.Id == yearId)
        .Select(s => s.Year).First();
    }

    private static string GetTeacherName(ref SchoolContext db, int yearId, int classId)
    {
      var teacherFullName = db.TeacherClasses.Where(e=> e.YearOfStudyId == yearId)
        .Where(e=> e.ClassId == classId)
        .Select(s => new {s.Teacher.Surname, s.Teacher.Name, s.Teacher.Second_name })
        .First();
      return teacherFullName.Surname + " " + teacherFullName.Name + " " + 
        teacherFullName.Second_name;
    }

    private static int GetAllPupils(ref SchoolContext db, int yearId, int classId)
    {
      return db.ClassPupils.Where(e => e.YearOfStudyId == yearId)
        .Where(e => e.ClassId == classId).Count();
    }

    private static byte GetFivePupils(ref SchoolContext db, int quarterId, int classId)
    {
      return (byte)db.ClassMathControls.Where(e => e.ClassId == classId &&
      e.QuarterId == quarterId).Select(s => s.MathControl.FiveGrade).First();
    }

    private static byte GetFourPupils(ref SchoolContext db, int quarterId, int classId)
    {
      return (byte)db.ClassMathControls.Where(e => e.ClassId == classId &&
      e.QuarterId == quarterId).Select(s => s.MathControl.FourGrade).First();
    }

    private static byte GetThreePupils(ref SchoolContext db, int quarterId, int classId)
    {
      return (byte)db.ClassMathControls.Where(e => e.ClassId == classId &&
      e.QuarterId == quarterId).Select(s => s.MathControl.ThreeGrade).First();
    }

    private static byte GetTwoPupils(ref SchoolContext db, int quarterId, int classId)
    {
      return (byte)db.ClassMathControls.Where(e => e.ClassId == classId &&
      e.QuarterId == quarterId).Select(s => s.MathControl.TwoGrade).First();
    }

    private static byte GetLogicTaskedPupils(ref SchoolContext db, int quarterId, int classId)
    {
      return (byte)db.ClassMathControls.Where(e => e.ClassId == classId &&
      e.QuarterId == quarterId).Select(s => s.MathControl.LogicTask).First();
    }

    private static int GetControlledPupils(int fivePupils, int fourPupils, int threePupils, int twoPupils)
    {
      return fivePupils + fourPupils + threePupils + twoPupils;
    }

    private static int GetNoLogicTaskedPupils(int cotrolledPupils, int logicTaskedPupils)
    {
      return cotrolledPupils - logicTaskedPupils;
    }

    private static double GetPerformance(int fivePupils, int fourPupils, int threePupils, int controlledPupils)
    {
      return Math.Round(((double)(fivePupils + fourPupils + threePupils) / controlledPupils * 100), 2);
    }

    private static double GetQualityPerformance(int fivePupils, int fourPupils, int controlledPupils)
    {
      return Math.Round(((double)(fivePupils + fourPupils) / controlledPupils * 100), 2);
    }

    private static double GetAverageScore(int fivePupils, int fourPupils, int threePupils, int twoPupils)
    {
      return Math.Round(((double)fivePupils * 5  + fourPupils * 4 + threePupils * 3 + twoPupils * 2)/
        (fivePupils + fourPupils + threePupils + twoPupils), 2);
    }
    //task id <3
    private static List<MathControlErrorAndCount> GetTaskErrorsCount(ref SchoolContext db, int mathControlId)
    {
      return db.MathControlErrors.Include(e => e.MathError)
        .Where(e => e.MathControlId == mathControlId && e.MathErrorId < 3)
        .Select(s => new MathControlErrorAndCount
        {
          ErrorName = s.MathError.Name,
          Count = s.ErrorCount
        }).ToList();
    }
    //calculateerrors id >2
    private static List<MathControlErrorAndCount> GetCalculateErrorsCount(ref SchoolContext db, int mathControlId)
    {
      return db.MathControlErrors.Include(e => e.MathError)
        .Where(e => e.MathControlId == mathControlId && e.MathErrorId > 2)
        .Select(s => new MathControlErrorAndCount
        {
          ErrorName = s.MathError.Name,
          Count = s.ErrorCount
        }).ToList();
    }
  }
}