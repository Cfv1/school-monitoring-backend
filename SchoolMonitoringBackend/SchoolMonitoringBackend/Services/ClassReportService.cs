﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SchoolMonitoringBackend.Models.LowSchoolReportModels;
using SchoolMonitoringBackend.Models;

namespace SchoolMonitoringBackend.Services
{
    public static class ClassReportService
    {
        //добавить второгодников
        static public ReportClassroom CreateReport(string className, int yearId)
        {
            SchoolContext db = new SchoolContext();
            int currentYear = Convert.ToInt32(db.YearOfStudies
                .Where(e => e.Id == yearId)
                .Select(s => s.Year).FirstOrDefault().Substring(0, 4));
            DateTime start_of_Year = new DateTime(currentYear, 9, 2);
            int classId = db.Classes.Where(e => e.Name == className).Select(s => s.Id).First();
            ReportClassroom report = FillReport(ref db, classId, yearId, className, start_of_Year);            
            return report;
        }

        static public ReportClassroom CreateReportByParallel(string className, int yearId)
        {
            SchoolContext db = new SchoolContext();
            var currentYear = Convert.ToInt32(db.YearOfStudies
                .Where(e => e.Id == yearId)
                .Select(s => s.Year).FirstOrDefault().Substring(0, 4));
            DateTime start_of_Year = new DateTime(currentYear, 9, 2);
            DateTime nextYear = start_of_Year.AddYears(1);
            ReportClassroom[] reports = new ReportClassroom[5];
            ReportClassroom finalReport = new ReportClassroom();
            int countFirstSeptember = 0; int men = 0; int women = 0; int countOuted = 0; int countInputed = 0;
            int countQuarterEndPupils = 0; int countAttestated = 0; int countOnlyFive = 0; int countJustOneFour = 0;
            int countWithoutThrees = 0; int countBadPupilsGeneral = 0; int countBadPupils1 = 0;
            int countBadPupils2 = 0; int countBadPupilsMore = 0; int countJustOneThreesPupils = 0;
            int countJustTwoThreesPupils = 0; double QualityPerformance = 0; double Performance = 0;
            byte quarter = 0;

            var classes = db.Classes.Where(e => e.Name.Contains(className.Replace(" ", string.Empty))).Select(s => new
            {
                Id = s.Id,
                Name = s.Name
            }).ToList();
            int classId = 0;
            for (var j = 0; j < 5; j++)
            {
                reports[j] = new ReportClassroom();
                classId = classes[j].Id;
                reports[j].ClassName = classes[j].Name;
                reports[j].First_september_pupils = GetFirstSeptemberPupils(ref db, start_of_Year, classId);
                reports[j].Men = GetMen(ref db, start_of_Year, classId);
                reports[j].Women = GetWomen(ref db, start_of_Year, classId);
                reports[j].Outed = GetOuted(ref db, classId, yearId);
                reports[j].Inputed = GetInputed(ref db, classId, yearId);
                reports[j].Quarter_end_pupils = GetQuarterEndPupils(reports[j].First_september_pupils,
                    ref reports[j].Inputed, ref reports[j].Outed);
                reports[j].Attestated_general = GetAttestatedGeneral(ref db, classId, yearId);
                reports[j].Onlyfive_pupils = GetOnlyFivePupils(ref db, classId, yearId);
                reports[j].Just_one_four_pupils = GetJustOneFourPupils(ref db, classId, yearId);
                reports[j].Without_threes = GetWithoutThreesPupils(ref db, classId, yearId);
                reports[j].Bad_pupils_general = GetBadPupilsGeneral(ref db, classId, yearId);
                reports[j].Bad_pupils_1 = GetBadPupils1(ref db, classId, yearId);
                reports[j].Bad_pupils_2 = GetBadPupils2(ref db, classId, yearId);
                reports[j].Bad_pupils_more = GetBadPupilsMore(ref db, classId, yearId);
                reports[j].Just_one_threes_pupils = GetJustOneThreePupils(ref db, classId, yearId);
                reports[j].Just_two_threes_pupils = GetJustTwoThreesPupils(ref db, classId, yearId);
                reports[j].QualityPerformance = GetQualityPerformance(ref db, ref reports[j].Without_threes,
                    ref reports[j].Quarter_end_pupils);
                reports[j].Performance = GetPerformance(ref db, ref reports[j].Attestated_general,
                    ref reports[j].Quarter_end_pupils);
            }

            for (var i = 0; i < 5; i++)
            {
                countFirstSeptember += reports[i].First_september_pupils;
                men += reports[i].Men;
                women += reports[i].Women;
                for (byte j = 0; j < 5; j++)
                {
                    countOuted += reports[j].Outed[quarter];
                    countInputed += reports[j].Outed[quarter];
                    countQuarterEndPupils += reports[j].Quarter_end_pupils[quarter];
                    countAttestated += reports[j].Attestated_general[quarter];
                    countOnlyFive += reports[j].Onlyfive_pupils[quarter];
                    countJustOneFour += reports[j].Just_one_four_pupils[quarter];
                    countWithoutThrees += reports[j].Without_threes[quarter];
                    countBadPupilsGeneral += reports[j].Bad_pupils_general[quarter];
                    countBadPupils1 += reports[j].Bad_pupils_1[quarter];
                    countBadPupils2 += reports[j].Bad_pupils_2[quarter];
                    countBadPupilsMore += reports[j].Bad_pupils_more[quarter];
                    countJustOneThreesPupils += reports[j].Just_one_threes_pupils[quarter];
                    countJustTwoThreesPupils += reports[j].Just_two_threes_pupils[quarter];
                    QualityPerformance += reports[j].QualityPerformance[quarter];
                    Performance += reports[j].Performance[quarter];
                }
                if (i < 3)
                {
                    quarter++;
                }
                finalReport.Outed.Add(countOuted);
                finalReport.Inputed.Add(countInputed);
                finalReport.Quarter_end_pupils.Add(countQuarterEndPupils);
                finalReport.Attestated_general.Add(countAttestated);
                finalReport.Onlyfive_pupils.Add(countOnlyFive);
                finalReport.Just_one_four_pupils.Add(countJustOneFour);
                finalReport.Without_threes.Add(countWithoutThrees);
                finalReport.Bad_pupils_general.Add(countBadPupilsGeneral);
                finalReport.Bad_pupils_1.Add(countBadPupils1);
                finalReport.Bad_pupils_2.Add(countBadPupils2);
                finalReport.Bad_pupils_more.Add(countBadPupilsMore);
                finalReport.Just_one_threes_pupils.Add(countJustOneThreesPupils);
                finalReport.Just_two_threes_pupils.Add(countJustTwoThreesPupils);
                finalReport.QualityPerformance.Add(QualityPerformance);
                finalReport.Performance.Add(Performance);
                countOuted = 0; countInputed = 0; countQuarterEndPupils = 0; countAttestated = 0;
                countOnlyFive = 0; countJustOneFour = 0; countWithoutThrees = 0; countBadPupilsGeneral = 0;
                countBadPupils1 = 0; countBadPupils2 = 0; countBadPupilsMore = 0; countJustOneThreesPupils = 0;
                countJustTwoThreesPupils = 0; QualityPerformance = 0; Performance = 0;
            }
            finalReport.Men = men;
            finalReport.First_september_pupils = countFirstSeptember;
            finalReport.Men = men;
            finalReport.Women = women;
            finalReport.ClassName = className.Substring(0, 1) + " классам";

            return finalReport;
        }

        //допилить даты
        static private int GetFirstSeptemberPupils(ref SchoolContext db, DateTime start_of_Year, int classId)
        {
            return db.Pupils.Where(t => t.ClassPupils.Any(c => c.ClassId == classId))
                .Where(t => t.Arrived.Date_of_arrival <= start_of_Year)
                .Where(t=> t.Retired.Date_of_retirement >= start_of_Year ||
                t.Retired == null)
                .Count();
        }

        static private int GetMen(ref SchoolContext db, DateTime start_of_Year, int classId)
        {
            return db.Pupils.Where(t => t.ClassPupils.Any(c => c.ClassId == classId))
                .Where(t => t.Arrived.Date_of_arrival <= start_of_Year && t.Gender == "Мужской")
                .Where(t => t.Retired.Date_of_retirement >= start_of_Year ||
                t.Retired == null)
                .Count();
        }

        static private int GetWomen(ref SchoolContext db, DateTime start_of_Year, int classId)
        {
            return db.Pupils.Where(t => t.ClassPupils.Any(c => c.ClassId == classId))
                .Where(t => t.Arrived.Date_of_arrival <= start_of_Year && t.Gender == "Женский")
                .Where(t => t.Retired.Date_of_retirement >= start_of_Year ||
                t.Retired == null)
                .Count();
        }

        static private List<int> GetOuted(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            for (var i = 0; i < 4; i++)
            {
                list.Add((db.Pupils.Include(t => t.Retired)
                    .Where(z => z.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(z => z.Retired.Quarter.Name == i + 1)
                    .Where(z => z.Retired.Date_of_retirement <= z.Retired.Quarter.Date_of_ending)
                    .Where(z => z.Retired.Quarter.YearOfStudyId == yearId)
                    .Where(z => z.Retired.Date_of_retirement > z.Retired.Quarter.Date_of_start))
                    .Count());
            }
            return list;
        }

        static private List<int> GetInputed(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            for (var i = 0; i < 4; i++)
            {
                list.Add((db.Pupils.Include(t => t.Arrived)
                    .Where(z => z.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(z => z.Arrived.Quarter.Name == i + 1)
                    .Where(z => z.Arrived.Quarter.YearOfStudyId == yearId)
                    .Where(z => z.Arrived.Date_of_arrival <= z.Arrived.Quarter.Date_of_ending)
                    .Where(z => z.Arrived.Date_of_arrival > z.Arrived.Quarter.Date_of_start)).Count());
            }
            return list;
        }

        static private List<int> GetQuarterEndPupils(int firstSeptemberPupils, ref List<int> inputed, ref List<int> outed)
        {
            List<int> list = new List<int>(5);
            var pupilEndQuarterCount = firstSeptemberPupils;
            for (var i = 0; i < 4; i++)
            {
                pupilEndQuarterCount += inputed[i] - outed[i];
                list.Add(pupilEndQuarterCount);
            }
            return list;
        }


        //subjectId надо че-то придумать
        static private List<int> GetAttestatedGeneral(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = (db.PupilGrades
                        .Include(t => t.Pupil)
                        .Include(t => t.Quarter)
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation != 2)
                        .Where(e => e.SubjectId == 1)
                        .Select(e => e.PupilId)
                        .Intersect(
                        db.PupilGrades
                        .Include(t => t.Pupil)
                        .Include(t => t.Quarter)
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation != 2)
                        .Where(e => e.SubjectId == 2)
                        .Select(e => e.PupilId))).Count();

                list.Add(count);
            }
            return list;
        }

        static private List<int> GetOnlyFivePupils(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = (db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.Evaluation == 5)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Select(e => e.PupilId)
                        .Except(
                        db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 4)
                        .Select(e => e.PupilId))
                        .Except(
                        db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 3)
                        .Select(e => e.PupilId))
                        .Except(
                        db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 2)
                        .Select(e => e.PupilId))).Count();

                list.Add(count);
            }
            return list;
        }
        static private List<int> GetJustOneFourPupils(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = (db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.Quarter.Name == i + 1)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation == 4)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() == 1)
                    .Select(s => s.Key)
                    .Intersect(
                    db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.Quarter.Name == i + 1)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation == 5)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() >= 1) //исправить на >1
                    .Select(s => s.Key))
                    ).Count();
                list.Add(count);
            }
            return list;
        }

        static private List<int> GetWithoutThreesPupils(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = (db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 5)
                        .Select(e => e.PupilId)
                        .Intersect(
                        db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 4)
                        .Select(e => e.PupilId))).Count();

                list.Add(count);
            }
            return list;
        }

        static private List<int> GetBadPupilsGeneral(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 2)
                        .Select(e => e.PupilId)
                        .Count();

                list.Add(count);
            }
            return list;
        }

        static private List<int> GetBadPupils1(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 2)
                        .GroupBy(grp => grp.PupilId)
                        .Where(e => e.Count() == 1)
                        .Select(s => s.Key)
                        .Count();

                list.Add(count);
            }
            return list;
        }

        static private List<int> GetBadPupils2(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 2)
                        .GroupBy(grp => grp.PupilId)
                        .Where(e => e.Count() == 2)
                        .Select(s => s.Key)
                        .Count();

                list.Add(count);
            }
            return list;
        }
        static private List<int> GetBadPupilsMore(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = db.PupilGrades
                        .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                        .Where(e => e.Quarter.Name == i + 1)
                        .Where(e => e.YearOfStudyId == yearId)
                        .Where(e => e.Evaluation == 2)
                        .GroupBy(grp => grp.PupilId)
                        .Where(e => e.Count() > 2)
                        .Select(s => s.Key)
                        .Count();

                list.Add(count);
            }
            return list;
        }

        static private List<int> GetJustOneThreePupils(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = (db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.Quarter.Name == i + 1)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation == 3)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() == 1)
                    .Select(s => s.Key)
                    .Intersect(
                    db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.Quarter.Name == i + 1)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation != 2)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() > 1)
                    .Select(s => s.Key))
                    ).Count();
                list.Add(count);
            }
            return list;
        }

        static private List<int> GetJustTwoThreesPupils(ref SchoolContext db, int classId, int yearId)
        {
            List<int> list = new List<int>(5);
            int count = 0;
            for (var i = 0; i < 4; i++)
            {
                count = (db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.Quarter.Name == i + 1)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation == 3)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() == 2)
                    .Select(s => s.Key)
                    .Intersect(
                    db.PupilGrades
                    .Where(e => e.Pupil.ClassPupils.Any(c => c.ClassId == classId))
                    .Where(e => e.Quarter.Name == i + 1)
                    .Where(e => e.YearOfStudyId == yearId)
                    .Where(e => e.Evaluation != 2)
                    .GroupBy(grp => grp.PupilId)
                    .Where(e => e.Count() > 1)
                    .Select(s => s.Key))
                    ).Count();
                list.Add(count);
            }
            return list;
        }

        static private List<double> GetQualityPerformance(ref SchoolContext db, ref List<int> withoutThrees, ref List<int> quarterEndPupils)
        {
            List<double> list = new List<double>(5);
            for (var i = 0; i < 4; i++)
            {
                var qualityPerformance = Math.Round(((double)withoutThrees[i] * 100
                        / quarterEndPupils[i]), 2);
                list.Add(qualityPerformance);
            }
            return list;
        }

        static private List<double> GetPerformance(ref SchoolContext db, ref List<int> attestatedGeneral, ref List<int> quarterEndPupils)
        {
            List<double> list = new List<double>(5);
            for (var i = 0; i < 4; i++)
            {
                var performance = Math.Round(((double)attestatedGeneral[i] * 100
                    / quarterEndPupils[i]), 2);
                list.Add(performance);
            }
            return list;
        }

        static private ReportClassroom FillReport(ref SchoolContext db, int classId, int yearId, string className, DateTime start_of_Year)
        {
          ReportClassroom report = new ReportClassroom();
          report.ClassName = className + " классу";
          report.First_september_pupils = GetFirstSeptemberPupils(ref db, start_of_Year, classId);
          report.Men = GetMen(ref db, start_of_Year, classId);
          report.Women = GetWomen(ref db, start_of_Year, classId);
          report.Outed = GetOuted(ref db, classId, yearId);
          report.Inputed = GetInputed(ref db, classId, yearId);
          report.Quarter_end_pupils = GetQuarterEndPupils(report.First_september_pupils,
              ref report.Inputed, ref report.Outed);
          report.Attestated_general = GetAttestatedGeneral(ref db, classId, yearId);
          report.Onlyfive_pupils = GetOnlyFivePupils(ref db, classId, yearId);
          report.Just_one_four_pupils = GetJustOneFourPupils(ref db, classId, yearId);
          report.Without_threes = GetWithoutThreesPupils(ref db, classId, yearId);
          report.Bad_pupils_general = GetBadPupilsGeneral(ref db, classId, yearId);
          report.Bad_pupils_1 = GetBadPupils1(ref db, classId, yearId);
          report.Bad_pupils_2 = GetBadPupils2(ref db, classId, yearId);
          report.Bad_pupils_more = GetBadPupilsMore(ref db, classId, yearId);
          report.Just_one_threes_pupils = GetJustOneThreePupils(ref db, classId, yearId);
          report.Just_two_threes_pupils = GetJustTwoThreesPupils(ref db, classId, yearId);
          report.QualityPerformance = GetQualityPerformance(ref db, ref report.Without_threes,
              ref report.Quarter_end_pupils);
          report.Performance = GetPerformance(ref db, ref report.Attestated_general,
              ref report.Quarter_end_pupils);
          return report;
    }


    }
}
