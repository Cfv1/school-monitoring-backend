﻿using System;
using System.Collections.Generic;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.AuxilliaryModels;
using SchoolMonitoringBackend.Models.LowSchoolReportModels;
using System.Data.Entity;
using System.Linq;

namespace SchoolMonitoringBackend.Services
{
  public static class StartYearReportService
  {
    public static StartYearReport CreateStartYearReport(int yearId, string className)
    {
      SchoolContext db = new SchoolContext();
      var currentyear = db.YearOfStudies.Find(yearId);
      var year = Convert.ToInt32(currentyear.Year.Substring(0, 4));
      DateTime startSummer = new DateTime(year, 6, 1);
      DateTime endSummer = new DateTime(year, 8, 31);
      StartYearReport report = new StartYearReport();
      report.ClassName = className;
      var clasData = db.TeacherClasses.Where(e => e.YearOfStudyId == yearId)
        .Where(e => e.Class.Name == className)
        .Select(s => new
        {
          Profile = s.Class.Profile,
          Cabinet = s.Cabinet,
          teacher = s.Teacher.Surname + " " + s.Teacher.Name + " " + s.Teacher.Second_name
        }).First();
      report.Teacher = clasData.teacher;
      report.Cabinet = clasData.Cabinet;
      report.Profile = clasData.Profile;
      report.Pupils = GetPupilsInClass(ref db, yearId, className);
      report.Birthdays = GetBirthYears(ref db, yearId, className);
      report.MenBirthdays = GetMenBirthYearCount(ref db, yearId, className, ref report.Birthdays);
      report.WomenBirthdays = GetWomenBirthYearCount(ref db, yearId, className, ref report.Birthdays);
      report.Retireds = GetRetireds(ref db, yearId, className, startSummer, endSummer);
      report.Arriveds = GetArriveds(ref db, yearId, className, startSummer, endSummer);
      report.HardFamilyPupils = GetHardListPupils(ref db, yearId, className);
      report.PartialFamilyPupils = GetPartialFamilyPupils(ref db, yearId, className);
      report.PupilData = GetStartYearPupilsData(ref db, yearId, className);
      report.EnglishStudyPupils = GetGroupStudyPupils(ref db, yearId, className, "Английский");
      report.GermanStudyPupils = GetGroupStudyPupils(ref db, yearId, className, "Немецкий");
      report.IVTFirstGroup = GetGroupStudyPupils(ref db, yearId, className, "ИВТ 1");
      report.IVTSecondGroup = GetGroupStudyPupils(ref db, yearId, className, "ИВТ 2");
      report.EnglishTeacher = GetGroupStudyTeacher(ref db, yearId, className, "Английский");
      report.GermanTeacher = GetGroupStudyTeacher(ref db, yearId, className, "Немецкий");
      report.IVTFirstGroupTeacher = GetGroupStudyTeacher(ref db, yearId, className, "ИВТ 1");
      report.IVTSeconGroupTeacher = GetGroupStudyTeacher(ref db, yearId, className, "ИВТ 2");
      return report;

    }

    private static List<string> GetPupilsInClass(ref SchoolContext db, int yearId, string className)
    {
      var list = new List<string>();
      var query = db.Pupils.Include(c => c.ClassPupils)
        .Where(e => e.ClassPupils.Any(c => c.Class.Name == className
       && c.YearOfStudyId == yearId))
       .Select(s => new
       {
         Surname = s.Surname,
         Name = s.Name,
         SecondName = s.Second_name
       });
      foreach (var q in query)
        list.Add(q.Surname + " " + q.Name + " " + q.SecondName);
      return list;
    }

    private static List<string> GetBirthYears(ref SchoolContext db, int yearId, string className)
    {
      string shortDateString = "";
      var list = new List<string>();
      var yearList = new List<string>();
      List<DateTime> birthList = db.Pupils.Where(i => i.ClassPupils.Any(c => c.Class.Name == className &&
         c.YearOfStudyId == yearId))
        .Select(e => e.Date_of_birth).ToList();
      foreach (var date in birthList)
      {
        shortDateString = date.ToShortDateString().Substring(6);
        if (!yearList.Contains(shortDateString))
          yearList.Add(shortDateString);
      }
      foreach (var year in yearList)
        list.Add(year);
      return list;
    }

    private static List<int> GetMenBirthYearCount(ref SchoolContext db, int yearId, string className, ref List<string> birthYearGeneral)
    {
      var list = new List<int>();
      var yearList = new List<string>();
      var menBirthDates = db.Pupils.Where(i => i.ClassPupils.Any(c => c.Class.Name == className &&
         c.YearOfStudyId == yearId))
         .Where(e => e.Gender == "Мужской")
         .Select(s => s.Date_of_birth)
         .ToList();
      foreach (var date in menBirthDates)
      {
        yearList.Add(date.ToShortDateString().Substring(6));
      }
      for (var i = 0; i < birthYearGeneral.Count; i++)
      {
        var count = 0;
        foreach (var year in yearList)
        {
          if (year == birthYearGeneral[i])
            count++;
        }
        list.Add(count);
      }
      var allBirthCount = 0;
      for (var i = 0; i < list.Count - 1; i++)
      {
        allBirthCount = list[i] + list[i + 1];
      }
      list.Add(allBirthCount);
      return list;
    }

    private static List<int> GetWomenBirthYearCount(ref SchoolContext db, int yearId, string className, ref List<string> birthYearGeneral)
    {
      var list = new List<int>();
      var yearList = new List<string>();
      var womenBirthDates = db.Pupils.Where(i => i.ClassPupils.Any(c => c.Class.Name == className &&
         c.YearOfStudyId == yearId))
         .Where(e => e.Gender == "Женский")
         .Select(s => s.Date_of_birth)
         .ToList();
      foreach (var date in womenBirthDates)
        yearList.Add(date.ToShortDateString().Substring(6));
      for (var i = 0; i < birthYearGeneral.Count; i++)
      {
        var count = 0;
        foreach (var year in yearList)
        {
          if (year == birthYearGeneral[i])
            count++;
        }
        list.Add(count);
      }
      var allBirthCount = 0;
      for (var i = 0; i < list.Count - 1; i++)
      {
        allBirthCount = list[i] + list[i + 1];
      }
      list.Add(allBirthCount);
      return list;
    }

    private static List<MovedPupil> GetRetireds(ref SchoolContext db, int yearId, string className, DateTime startSummer, DateTime endSummer)
    {
      return db.Pupils.Where(e => e.ClassPupils.Any(c => c.Class.Name == className
        && c.YearOfStudyId == yearId))
        .Where(e => e.Retired.Date_of_retirement >= startSummer &&
        e.Retired.Date_of_retirement <= endSummer)
        .Select(s => new MovedPupil
        {
          Surname = s.Surname,
          Name = s.Name,
          Place = s.Retired.Place
        })
        .ToList();
    }

    private static List<SummerArrived> GetArriveds(ref SchoolContext db, int yearId, string className, DateTime startSummer, DateTime endSummer)
    {
      var list = db.Pupils.Where(e => e.ClassPupils.Any(c => c.Class.Name == className
        && c.YearOfStudyId == yearId))
        .Where(e => e.Arrived.Date_of_arrival >= startSummer &&
        e.Arrived.Date_of_arrival <= endSummer)
        .Select(s => new SummerArrived
        {
          Surname = s.Surname,
          Name = s.Name,
          SecondName = s.Second_name,
          Place = s.Arrived.Place,
          Parents = s.PupilParents.Where(e => e.PupilId == s.Id)
          .Select(a => a.Parent).ToList()
        }).ToList();
      return list;
    }
    private static List<string> GetPartialFamilyPupils(ref SchoolContext db, int yearId, string className)
    {
      List<string> list = new List<string>();
      var query = db.Pupils.Where(e => e.PartialFamily == true)
        .Where(e => e.ClassPupils.Any(c => c.Class.Name == className
        && c.YearOfStudyId == yearId))
        .Select(s => new
        {
          Surname = s.Surname,
          Name = s.Name,
          SecondName = s.Second_name
        });
      foreach (var q in query)
        list.Add(q.Surname + " " + q.Name + " " + q.SecondName);
      return list;
    }

    private static List<string> GetHardListPupils(ref SchoolContext db, int yearId, string className)
    {
      List<string> list = new List<string>();
      var query = db.Pupils.Where(e => e.HardFamily == true)
        .Where(e => e.ClassPupils.Any(c => c.Class.Name == className
        && c.YearOfStudyId == yearId))
        .Select(s => new
        {
          Surname = s.Surname,
          Name = s.Name,
          SecondName = s.Second_name
        });
      foreach (var q in query)
        list.Add(q.Surname + " " + q.Name + " " + q.SecondName);
      return list;
    }

    private static List<StartYearPupilData> GetStartYearPupilsData(ref SchoolContext db, int yearId, string className)
    {
      var list = db.Pupils
        .Where(e => e.ClassPupils.Any(c => c.Class.Name == className
        && c.YearOfStudyId == yearId))
        .Select(s => new StartYearPupilData
        {
          Surname = s.Surname,
          Name = s.Name,
          SecondName = s.Second_name,
          BirthDate = s.Date_of_birth,
          Parents = s.PupilParents.Where(e => e.PupilId == s.Id)
          .Select(a => a.Parent).ToList()
        }).ToList();
      return list;
    }

    private static List<string> GetGroupStudyPupils(ref SchoolContext db, int yearId, string className, string groupName)
    {
      List<string> list = new List<string>();
      var query = db.PupilSubjectGroups.Include(i=> i.Pupil)
        .Include(i=> i.Pupil.ClassPupils).Include(i=> i.Group).Include(i=> i.Subject)
        .Where(e => e.Pupil.ClassPupils.Any(c => c.Class.Name == className
        && c.YearOfStudyId == yearId))
        .Where(e=> e.YearOfStudyId == yearId)
        .Where(e=> e.Group.Name.Contains(groupName))
        .Select(s => new
        {
          Surname = s.Pupil.Surname,
          Name = s.Pupil.Name,
          SecondName = s.Pupil.Second_name
        });
      foreach (var q in query)
        list.Add(q.Surname + " " + q.Name + " " + q.SecondName);
      return list;
    }

    private static string GetGroupStudyTeacher(ref SchoolContext db, int yearId, string className, string groupName)
    {
      var teacher = db.TeacherGroups.Where(e => e.YearOfStudyId == yearId)
        .Where(e => e.Class.Name == className)
        .Where(e => e.Group.Name.Contains(groupName))
        .Select(s=> new
        {
          s.Teacher.Name, s.Teacher.Surname, s.Teacher.Second_name
        })
        .Single();
      return teacher.Surname + " " + teacher.Name + " " + teacher.Second_name;
    }
  }
}
