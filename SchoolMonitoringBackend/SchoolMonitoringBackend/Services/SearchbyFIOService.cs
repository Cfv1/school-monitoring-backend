﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SchoolMonitoringBackend.Models;
using SchoolMonitoringBackend.Models.TeacherPanelModels;

namespace SchoolMonitoringBackend.Services
{
    public static class SearchbyFIOService
    {
        public static List<SearchbyFIO> Find(string fullname)
        {
            using (var db = new SchoolContext())
            {
                var teachers = new List<SearchbyFIO>();
                string[] parseFullName = fullname.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (fullname.Contains(" "))
                {
                    string name = parseFullName[1], surname = parseFullName[0];
          teachers = (from teacher in db.Teachers
                      where teacher.Surname == surname && teacher.Name == name
                      join educteach in db.EducationTeachers on teacher.Id equals educteach.TeacherId
                      join education in db.Educations on educteach.EducationId equals education.Id
                      join attestation in db.Attestations on teacher.AttestationId equals attestation.Id
                      join teacherreward in db.TeacherRewards on teacher.Id equals teacherreward.TeacherId
                      join rewards in db.Rewards on teacherreward.RewardId equals rewards.Id
                      select new SearchbyFIO
                      {
                        Name = teacher.Name,
                        Surname = teacher.Surname,
                        Second_name = teacher.Second_name,
                        Education = education.Name,
                        AttestationDate = attestation.Date,
                        CathegoryName = attestation.Name,
                        //Rewards.Add(rewards.Name)                                   
                       }).ToList();
                }
                else if (!fullname.Contains(" "))
                {
                    string surname = parseFullName[0];
                    teachers = (from teacher in db.Teachers
                                where teacher.Surname == surname
                                join educationteachers in db.EducationTeachers on teacher.Id equals educationteachers.TeacherId
                                join education in db.Educations on educationteachers.EducationId equals education.Id
                                join attestation in db.Attestations on teacher.AttestationId equals attestation.Id                               
                                select new SearchbyFIO
                                {                                    
                                    Name = teacher.Name,
                                    Surname = teacher.Surname,
                                    Second_name = teacher.Second_name,
                                    Education = education.Name,
                                    AttestationDate = attestation.Date,
                                    CathegoryName = attestation.Name
                                }
                        ).ToList();
                }
                return teachers;
            }
        }
    }
}